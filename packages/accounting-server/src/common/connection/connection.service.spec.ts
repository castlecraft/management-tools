import { Test, TestingModule } from '@nestjs/testing';
import { ConnectionService } from './connection.service';
import { getConnectionToken } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { Type } from '@nestjs/common';

describe('ConnectionService', () => {
  let service: ConnectionService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ConnectionService,
        {
          provide: getConnectionToken('mongodb') as Type<Connection>,
          useValue: {},
        },
        {
          provide: getConnectionToken('postgres') as Type<Connection>,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<ConnectionService>(ConnectionService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
