import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { Connection } from 'typeorm';

@Injectable()
export class ConnectionService {
  constructor(
    @InjectConnection('mongodb')
    private readonly mongodbConnection: Connection,
    @InjectConnection('postgres')
    private readonly postgresdbConnection: Connection,
  ) {}

  getMongoConnection() {
    return this.mongodbConnection;
  }

  getPostgresConnection() {
    return this.postgresdbConnection;
  }
}
