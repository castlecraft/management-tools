import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateGeneralLedgerCommand } from './update-general-ledger.command';
import { GeneralLedgerManagerAggregateService } from '../..//aggregates/general-ledger-manager-aggregate/general-ledger-manager-aggregate.service';

@CommandHandler(UpdateGeneralLedgerCommand)
export class UpdateGeneralLedgerHandler
  implements ICommandHandler<UpdateGeneralLedgerCommand> {
  constructor(
    private readonly manager: GeneralLedgerManagerAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: UpdateGeneralLedgerCommand) {
    const {
      generalLedgerPayload,
      transactionDate,
      journalEntryTransactionId,
    } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);

    await this.manager.updateGeneralLedger(
      generalLedgerPayload,
      transactionDate,
      journalEntryTransactionId,
    );
    aggregate.commit();
  }
}
