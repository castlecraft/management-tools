import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { BulkCsvAccountsCommand } from './bulk-accounts-csv.command';
import { BulkCsvAccountsManagerAggregateService } from '../../aggregates';

@CommandHandler(BulkCsvAccountsCommand)
export class BulkAccountsCsvCommandHandler
  implements ICommandHandler<BulkCsvAccountsCommand> {
  constructor(
    private readonly manager: BulkCsvAccountsManagerAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: BulkCsvAccountsCommand) {
    const { csvFileData, clientActor } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.bulkCsvForAccounts(csvFileData, clientActor).toPromise();
    aggregate.commit();
  }
}
