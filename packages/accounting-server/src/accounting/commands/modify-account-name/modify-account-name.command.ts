import { ICommand } from '@nestjs/cqrs';

export class ModifyAccountNameCommand implements ICommand {
  constructor(
    public readonly accountName: string,
    public readonly uuid: string,
    public readonly modifiedBy: string,
  ) {}
}
