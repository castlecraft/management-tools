import { ICommandHandler, EventPublisher, CommandHandler } from '@nestjs/cqrs';
import { ModifyAccountNameCommand } from './modify-account-name.command';
import { AccountManagerAggregate } from '../../../accounting/aggregates/account-manager-aggregate/account-manager-aggregate.service';

@CommandHandler(ModifyAccountNameCommand)
export class ModifyAccountNameHandler
  implements ICommandHandler<ModifyAccountNameCommand> {
  constructor(
    private readonly manager: AccountManagerAggregate,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: ModifyAccountNameCommand) {
    const { accountName, uuid, modifiedBy } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateAccountName(accountName, uuid, modifiedBy);
    aggregate.commit();
  }
}
