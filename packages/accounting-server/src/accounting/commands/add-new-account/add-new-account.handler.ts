import { ICommandHandler, EventPublisher, CommandHandler } from '@nestjs/cqrs';
import { AddNewAccountCommand } from './add-new-account.command';
import { AccountManagerAggregate } from '../../aggregates/account-manager-aggregate/account-manager-aggregate.service';

@CommandHandler(AddNewAccountCommand)
export class AddNewAccountHandler
  implements ICommandHandler<AddNewAccountCommand> {
  constructor(
    private readonly manager: AccountManagerAggregate,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: AddNewAccountCommand) {
    const { payload, req } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.addNewAccount(payload, req);
    aggregate.commit();
  }
}
