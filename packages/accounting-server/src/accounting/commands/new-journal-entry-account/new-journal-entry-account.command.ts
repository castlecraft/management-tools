import { ICommand } from '@nestjs/cqrs';

export class NewJournalEntryAccountCommand implements ICommand {
  constructor(
    public readonly payload: any,
    public readonly req: any[],
    public readonly requestType?: any,
  ) {}
}
