import { AddNewAccountHandler } from './add-new-account/add-new-account.handler';
import { ModifyAccountNameHandler } from './modify-account-name/modify-account-name.handler';
import { NewJournalEntryAccountHandler } from './new-journal-entry-account/new-journal-entry-account.handler';
import { UpdateGeneralLedgerHandler } from './update-general-ledger/update-general-ledger.handler';
import { BulkAccountsCsvCommandHandler } from './bulk-accounts-csv/bulk-accounts-csv.handler';

export const AccountManagementCommandHandlers = [
  ModifyAccountNameHandler,
  AddNewAccountHandler,
  NewJournalEntryAccountHandler,
  UpdateGeneralLedgerHandler,
  BulkAccountsCsvCommandHandler,
];
