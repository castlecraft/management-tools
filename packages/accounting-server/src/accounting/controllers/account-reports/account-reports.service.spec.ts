import { Test, TestingModule } from '@nestjs/testing';
import { AccountReportsService } from './account-reports.service';
import { AccountService } from '../../entities/account/account.service';
import { TreeService } from '../tree/tree.service';

describe('AccountReportsService', () => {
  let service: AccountReportsService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountReportsService,
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: TreeService,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<AccountReportsService>(AccountReportsService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
