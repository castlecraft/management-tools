import { IsDate, IsString } from 'class-validator';

export class ValidateReport {
  @IsString()
  @IsDate()
  fromTime: Date;

  @IsString()
  @IsDate()
  toTime: Date;
}
