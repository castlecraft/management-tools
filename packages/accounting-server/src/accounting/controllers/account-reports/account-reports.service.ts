import { Injectable } from '@nestjs/common';
import { AccountService } from '../../../accounting/entities/account/account.service';
import { from, of } from 'rxjs';
import { toArray, mergeMap, map, combineAll } from 'rxjs/operators';
import { Account } from '../../entities/account/account.entity';
import * as ObjectsToCsv from 'objects-to-csv';
import { CREDIT, PROFIT_ACCOUNTS } from '../../../constants/app-strings';
import { TreeService } from '../tree/tree.service';

@Injectable()
export class AccountReportsService {
  ObjectsToCsv = require('objects-to-csv');
  constructor(
    private readonly accountService: AccountService,
    private readonly treeService: TreeService,
  ) {}

  getAccountClosing(fromTime, toTime, accountId) {
    return from(
      this.accountService.getRepository().query(
        `
    SELECT "accountId","amountType" , COALESCE(SUM(amount), 0) AS Total
    FROM (SELECT * FROM general_ledger
    WHERE "transactionDate" >= $1 AND "transactionDate" < $2) AS FOO
    WHERE "accountId" = $3 GROUP BY "amountType" , "accountId"
      `,
        [fromTime, toTime, accountId],
      ),
    );
  }

  getAccountsByTime(fromTime, toTime, accountTypes?) {
    let query = `
    SELECT id ,"accountNumber" ,"uuid" ,"accountType" FROM account
    WHERE id IN (SELECT "accountId" as account
    FROM (SELECT* , ROW_NUMBER() OVER(PARTITION BY "accountId" )
    AS RN FROM general_ledger)A WHERE RN=1 AND "transactionDate"
    BETWEEN $1 AND $2)
    `;

    if (accountTypes) {
      let iterator = 0;
      query += 'and (';
      accountTypes.forEach(element => {
        iterator++;
        if (iterator < accountTypes.length)
          query += ` "accountType" = '${element}' or`;
        else query += ` "accountType" = '${element}'`;
      });
      query.slice(0, -2);
      query += ');';
    }
    return from(
      this.accountService.getRepository().query(query, [fromTime, toTime]),
    );
  }

  getReportByTime(fromTime, toTime, accountTypes?) {
    return from(this.getAccountsByTime(fromTime, toTime, accountTypes)).pipe(
      mergeMap((data: any) => {
        return from(data).pipe(
          mergeMap((account: Account) => {
            return this.getAccountClosing(fromTime, toTime, account.id).pipe(
              mergeMap((asset: any) => {
                return this.amountHandler(account, asset);
              }),
            );
          }),
        );
      }),
      mergeMap((response: any) => {
        return of<any>(response).pipe(
          mergeMap(report => {
            return this.getPeriodClosing(fromTime, toTime, report.Id).pipe(
              mergeMap(data => {
                response.openingBalance = data[0].opening;
                response.closingBalance = data[0].closing;
                return of(response);
              }),
            );
          }),
        );
      }),
      toArray(),
    );
  }

  getPeriodClosing(opening, closing, id) {
    return from(
      this.accountService.getRepository().query(
        `
      SELECT * FROM (SELECT
        (SELECT COALESCE(SUM("amount") , 0)
        FROM general_ledger WHERE "accountId" = $3
        AND "transactionDate" < $1
        AND "amountType" = 'DEBIT' )
        -
        (SELECT COALESCE(SUM("amount") , 0)
        FROM general_ledger WHERE "accountId" = 8
        AND "transactionDate" < $1
        AND "amountType" = 'CREDIT' )
        as opening )a,
        (SELECT
        (SELECT COALESCE(SUM("amount") , 0)
        FROM general_ledger WHERE "accountId" = $3
        AND "transactionDate" < $2
        AND "amountType" = 'DEBIT' )
        -
        (SELECT COALESCE(SUM("amount") , 0)
        FROM general_ledger WHERE "accountId" = $3
        AND "transactionDate" < $2
        AND "amountType" = 'CREDIT' )
        AS closing)b
      `,
        [opening, closing, id],
      ),
    );
  }

  getExpenditureReports(fromTime, toTime) {}

  async sendProfitReportFile(data) {
    function sortByKey(array, key) {
      return array.sort((a, b) => {
        const x = a[key];
        const y = b[key];
        return x < y ? -1 : x > y ? 1 : 0;
      });
    }
    data = sortByKey(data, 'accountType');
    return await this.sendReportFile(data);
  }

  async sendReportFile(data) {
    return await new ObjectsToCsv(data).toString();
  }

  getProfitReports(fromTime, toTime) {
    this.treeService;
    return from(PROFIT_ACCOUNTS).pipe(
      mergeMap(accounts => {
        return this.treeService.getAllChildren(accounts).pipe(
          mergeMap(accountId => {
            accountId.splice(0, 1);
            return from(accountId).pipe(
              mergeMap((accountIdLoop: Account) => {
                return this.getAccountData(
                  accountIdLoop.accountNumber,
                  fromTime,
                  toTime,
                ).pipe(
                  mergeMap(accountData => {
                    return this.getPeriodClosing(
                      fromTime,
                      toTime,
                      accountData[0].Id,
                    ).pipe(closing => {
                      return closing.pipe(
                        map(closingData => {
                          accountData[0].openingBalance =
                            closingData[0].opening;
                          accountData[0].closingBalance =
                            closingData[0].closing;
                          return accountData;
                        }),
                      );
                    });
                  }),
                );
              }),
            );
          }),
        );
      }),
      combineAll(),
    );
  }

  getAccountData(accountId, fromTime, toTime) {
    return from(
      this.accountService.getRepository().query(
        `
      SELECT *
      FROM account
      WHERE id = $1
      `,
        [accountId],
      ),
    ).pipe(
      mergeMap(accountData => {
        return from(this.getAccountClosing(fromTime, toTime, accountId)).pipe(
          mergeMap(sum => {
            return of(this.amountHandler(accountData[0], sum));
          }),
        );
      }),
    );
  }

  amountHandler(account, asset) {
    const report: any = [{ accountNumber: account.accountNumber }];
    report[0].Id = account.id;
    if (account.accountName) report[0].accountName = account.accountName;
    report[0].accountType = account.accountType;
    report[0].uuid = account.uuid;
    if (asset.length === 1) {
      if (asset[0].amountType === CREDIT) {
        report[0].creditAmount = asset[0].total;
        report[0].debitAmount = 0;
      } else {
        report[0].creditAmount = 0;
        report[0].debitAmount = asset[0].total;
      }
      return report;
    } else {
      report[0].creditAmount = asset[0].total;
      report[0].debitAmount = asset[1].total;
      return report;
    }
  }
}
