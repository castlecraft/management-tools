import { Test, TestingModule } from '@nestjs/testing';
import { AccountReportsController } from './account-reports.controller';
import { AccountReportsService } from './account-reports.service';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { SettingsService } from '../../../system-settings/settings/settings.service';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { HttpService } from '@nestjs/common';

describe('AccountReports Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [AccountReportsController],
      providers: [
        {
          provide: AccountReportsService,
          useValue: {
            getProfitReport: (...args) => {},
          },
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: TokenCacheService,
          useValue: {},
        },
        {
          provide: HttpService,
          useFactory: (...args) => jest.fn(),
        },
      ],
    })
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();
  });

  it('should be defined', () => {
    const controller: AccountReportsController = module.get<
      AccountReportsController
    >(AccountReportsController);
    expect(controller).toBeDefined();
  });
});
