import { Test, TestingModule } from '@nestjs/testing';
import { TreeController } from './tree.controller';
import { TreeService } from './tree.service';
import { AuthServerVerificationGuard } from '../../../auth/guards/authserver-verification.guard';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { SettingsService } from '../../../system-settings/settings/settings.service';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { HttpService } from '@nestjs/common';

describe('Tree Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [TreeController],
      providers: [
        {
          provide: TreeService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: TokenCacheService,
          useValue: {},
        },
        {
          provide: HttpService,
          useFactory: (...args) => jest.fn(),
        },
      ],
    })
      .overrideGuard(AuthServerVerificationGuard)
      .useValue({})
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();
  });
  it('should be defined', () => {
    const controller: TreeController = module.get<TreeController>(
      TreeController,
    );
    expect(controller).toBeDefined();
  });
});
