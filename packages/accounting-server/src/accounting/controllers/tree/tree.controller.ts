import { Controller, Get, UseGuards, Param } from '@nestjs/common';
import { TreeService } from './tree.service';
import { AccountDto } from '../../controllers/account/account-dto';
import { from } from 'rxjs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ACCOUNTANT } from '../../../constants/roles';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { RoleGuard } from '../../../auth/guards/role.guard';

@Controller('tree')
export class TreeController {
  constructor(private readonly treeService: TreeService) {}

  @Get('v1/getRoots')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  getRoots() {
    return this.treeService.getRoots();
  }

  @Get('v1/:accountNumber')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  getTree(@Param('accountNumber') accountNumber: AccountDto) {
    const account = accountNumber.toString();
    // let accountType = null;
    // if (account in ACCOUNT_TYPES) {
    //   switch (account) {
    //     case ASSET:
    //       accountType = account;
    //       accountNumber = null;
    //       break;
    //     case LIABILITY:
    //       accountType = account;
    //       accountNumber = null;
    //       break;
    //     case EQUITY:
    //       accountType = account;
    //       accountNumber = null;
    //       break;
    //     case INCOME:
    //       accountType = account;
    //       accountNumber = null;
    //       break;
    //     case EXPENSE:
    //       accountType = account;
    //       accountNumber = null;
    //       break;
    //   }
    // }
    return from(this.treeService.getChildren(account));
  }
}
