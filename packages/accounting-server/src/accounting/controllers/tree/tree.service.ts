import { Injectable, BadRequestException } from '@nestjs/common';
import { of, from } from 'rxjs';
import { mergeMap, toArray, catchError } from 'rxjs/operators';
import { AccountService } from '../../../accounting/entities/account/account.service';
import { ACCOUNT_TYPES } from '../../../constants/app-strings';

@Injectable()
export class TreeService {
  constructor(private readonly accountService: AccountService) {}
  accountChildren: string[] = [];

  getAccount(data, type) {
    return from(
      this.accountService.getRepository().query(
        `
    SELECT * from account where id = $1 or ("parentId" is null and "accountType" = $2);
    `,
        [data, type],
      ),
    );
  }
  getSum(accountNumber) {
    return this.getAllChildren(accountNumber).pipe(
      mergeMap(data => {
        return from(data).pipe(
          mergeMap((id: any) => {
            return from(
              this.accountService.getRepository().query(
                `
              SELECT COALESCE(SUM(amount),0) as total FROM general_ledger where "amountType" = 'CREDIT' and "accountId" = $1
              `,
                [id.id_ancestor],
              ),
            ).pipe(
              mergeMap(creditAmount => {
                return from(
                  this.accountService.getRepository().query(
                    `
                    SELECT COALESCE(SUM(amount),0) as total FROM general_ledger WHERE "accountId" IN
                    (SELECT id from account where "isGroup" = false AND "accountType" = $1) and "amountType" = 'DEBIT'
                    `,
                    [id.id_ancestor],
                  ),
                ).pipe(
                  mergeMap(debitAmount => {
                    const amount =
                      debitAmount[0].total - creditAmount[0].total || 0;
                    id.sum = amount;
                    return of(id);
                  }),
                );
              }),
            );
          }),
          toArray(),
        );
      }),
      toArray(),
    );
  }

  getChildren(accountNumber) {
    return this.getId(accountNumber).pipe(
      mergeMap(id => {
        return from(
          this.accountService.getRepository().query(
            `
        SELECT id_ancestor FROM account_closure WHERE id_descendant = $1
        AND id_ancestor != id_descendant
        `,
            [id[0].id],
          ),
        ).pipe(
          mergeMap(accountId => {
            return from(accountId).pipe(
              mergeMap((account: any) => {
                return from(
                  this.accountService.getRepository().query(
                    `
                SELECT * FROM account WHERE id = $1
                `,
                    [account.id_ancestor],
                  ),
                ).pipe(
                  mergeMap(accountData => {
                    return from(
                      this.accountService.getRepository().query(
                        `
                      SELECT COALESCE(SUM(amount),0) as total FROM general_ledger where "amountType" = 'CREDIT' and "accountId" = $1
                      `,
                        [accountData.id],
                      ),
                    ).pipe(
                      mergeMap(creditAmount => {
                        return from(
                          this.accountService.getRepository().query(
                            `
                            SELECT COALESCE(SUM(amount),0) as total FROM general_ledger WHERE "amountType" = 'DEBIT' and "accountId" = $1
                            `,
                            [accountData.id],
                          ),
                        ).pipe(
                          mergeMap(debitAmount => {
                            accountData[0].creditAmount = creditAmount[0].total;
                            accountData[0].debitAmount = debitAmount[0].total;
                            const amount =
                              debitAmount[0].total - creditAmount[0].total || 0;
                            accountData[0].totalBalance = amount;
                            return of(accountData[0]);
                          }),
                        );
                      }),
                    );
                  }),
                );
              }),
            );
          }),
        );
      }),
      toArray(),
    );
  }

  getAllChildren(accountNumber) {
    return this.getId(accountNumber).pipe(
      mergeMap(id => {
        return from(
          this.accountService.getRepository().query(
            `WITH RECURSIVE subordinates AS (
              SELECT s.id_ancestor, s.id_descendant FROM account_closure s WHERE s.id_ancestor = $1 UNION
              SELECT e.id_ancestor, e.id_descendant FROM account_closure e
              INNER JOIN subordinates s ON s.id_ancestor = e.id_descendant
             ) SELECT id_ancestor as "accountNumber" FROM subordinates`,
            [id[0].id],
          ),
        ).pipe(
          mergeMap(children => {
            return children;
          }),
        );
      }),
      catchError(error => {
        throw new BadRequestException('Invalid Account');
      }),
      toArray(),
    );
  }

  async getRoots() {
    return from(ACCOUNT_TYPES).pipe(
      mergeMap(type => {
        return from(
          this.accountService.getRepository().query(
            `
          SELECT COALESCE(SUM(amount),0) as total FROM general_ledger WHERE "accountId" IN
          (SELECT id from account where "isGroup" = false AND "accountType" = $1) and "amountType" = 'CREDIT'
          `,
            [type],
          ),
        ).pipe(
          mergeMap(creditAmount => {
            return from(
              this.accountService.getRepository().query(
                `
                SELECT COALESCE(SUM(amount),0) as total FROM general_ledger WHERE "accountId" IN
                (SELECT id from account where "isGroup" = false AND "accountType" = $1) and "amountType" = 'DEBIT'
                `,
                [type],
              ),
            ).pipe(
              mergeMap(debitAmount => {
                return from(
                  this.accountService.getRepository().query(
                    `
                SELECT id , "uuid", "accountNumber", "accountName", "accountType", "isGroup", "isRoot","parentId"
                FROM account WHERE "isRoot" = true and "accountType" = $1
                `,
                    [type],
                  ),
                ).pipe(
                  mergeMap(data => {
                    data[0].creditAmount = creditAmount[0].total;
                    data[0].debitAmount = debitAmount[0].total;
                    data[0].totalBalance =
                      debitAmount[0].total - creditAmount[0].total;
                    return data;
                  }),
                );
              }),
            );
          }),
        );
      }),
      toArray(),
    );
  }

  getId(accountNumber) {
    return from(
      this.accountService.getRepository().query(
        `
    SELECT id FROM account WHERE "accountNumber" = $1
  `,
        [accountNumber],
      ),
    ).pipe(
      mergeMap(data => {
        if (data.length === 0) {
          throw new BadRequestException('Invalid Account Number');
        } else {
          return of(data);
        }
      }),
    );
  }

  getAccountId(type) {
    return from(
      this.accountService.getRepository().query(
        `
    select id from account where "parentId" is null and "accountType" = $1;
  `,
        [type],
      ),
    );
  }
}
