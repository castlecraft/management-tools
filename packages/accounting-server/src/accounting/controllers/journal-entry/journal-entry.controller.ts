import {
  Controller,
  Post,
  Param,
  Body,
  Get,
  Query,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Req,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { JournalEntryManagementService } from './journal-entry-management.service';
import { JournalEntryDto } from './journal-entry-parent.dto';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { BULK_JSON, BULK_CSV } from '../../../constants/app-strings';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { ACCOUNTANT } from '../../../constants/roles';

@Controller('journalentry')
export class JournalEntryController {
  constructor(
    private readonly managementService: JournalEntryManagementService,
  ) {}

  @Post('v1/create')
  @Roles(ACCOUNTANT)
  @UseGuards(TokenGuard, RoleGuard)
  @UsePipes(ValidationPipe)
  createEntry(@Body() payload: JournalEntryDto, @Req() req) {
    const requestType = BULK_JSON;
    return this.managementService.create(payload, req, requestType);
  }

  @Post('v1/bulkCsv')
  @Roles(ACCOUNTANT)
  @UseGuards(TokenGuard, RoleGuard)
  @UsePipes(ValidationPipe)
  @UseInterceptors(FileInterceptor('file'))
  createBulkCsvJournalEntry(
    @UploadedFile('file') file,
    @Req() clientHttpRequest,
  ) {
    const requestType = BULK_CSV;
    return this.managementService.create(file, clientHttpRequest, requestType);
  }

  @Get('v1/getOne/:id')
  @Roles(ACCOUNTANT)
  @UseGuards(TokenGuard, RoleGuard)
  async getOne(
    @Param('id') id: number,
    @Req() req,
    @Query('limit') limit: number = 10,
    @Query('offset') offset: number = 0,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    const query: { createdBy?: string } = {};

    // if (!(await this.userService.checkAdministrator(req.user.user))) {
    //   query.createdBy = req.user.utser;
    // }
    sort = sort ? sort.toUpperCase() : 'ASC';

    return await this.managementService.getOne(
      id,
      limit,
      offset,
      search,
      sort,
      query,
    );
  }

  @Get('v1/list/all')
  @Roles(ACCOUNTANT)
  @UseGuards(TokenGuard, RoleGuard)
  async listEntries(
    @Query('limit') limit: number = 10,
    @Query('offset') offset: number = 0,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    const query: { createdBy?: string } = {};

    // if (!(await this.userService.checkAdministrator(req.user.user))) {
    //   query.createdBy = req.user.utser;
    // }
    sort = sort ? sort.toUpperCase() : 'ASC';
    return await this.managementService.getAll(
      limit,
      offset,
      search,
      sort,
      query,
    );
  }
}
