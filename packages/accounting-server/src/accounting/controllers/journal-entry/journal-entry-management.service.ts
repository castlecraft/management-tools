import { Injectable } from '@nestjs/common';
import { JournalEntryService } from '../../../accounting/entities/journal-entry/journal-entry.service';
import { JournalEntryAccountService } from '../../../accounting/entities/journal-entry-account/journal-entry-account.service';
import { AccountService } from '../../../accounting/entities/account/account.service';
import { CommandBus } from '@nestjs/cqrs';
import { NewJournalEntryAccountCommand } from '../../commands/new-journal-entry-account/new-journal-entry-account.command';

@Injectable()
export class JournalEntryManagementService {
  constructor(
    private journalEntryService: JournalEntryService,
    private journalEntryAccountService: JournalEntryAccountService,
    private accountService: AccountService,
    private readonly commandBus: CommandBus,
  ) {}

  async getAll(limit, offset, search, sort, query) {
    return await this.journalEntryService.getAll(
      limit,
      offset,
      search,
      sort,
      query,
    );
  }

  async getOne(id, limit, offset, search, sort, query) {
    const accounts = await this.journalEntryAccountService
      .getRepository()
      .query(
        `SELECT "entryType" , "amount" , "accountId" from journal_entry_account where "journalEntryTransactionId" = $1`,
        [id],
      );

    for (const account of accounts) {
      const accData = await this.accountService
        .getRepository()
        .query('SELECT "accountNumber" FROM "account" where id = $1', [
          account.accountId,
        ]);
      account.account = accData[0].accountNumber;
    }

    return accounts;
  }

  async create(payload, req, requestType) {
    return await this.commandBus.execute(
      new NewJournalEntryAccountCommand(payload, req, requestType),
    );
  }
}
