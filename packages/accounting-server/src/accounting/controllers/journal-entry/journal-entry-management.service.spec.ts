import { Test, TestingModule } from '@nestjs/testing';
import { JournalEntryManagementService } from './journal-entry-management.service';
import { AccountService } from '../../entities/account/account.service';
import { JournalEntryService } from '../../entities/journal-entry/journal-entry.service';
import { GeneralLedgerService } from '../../entities/general-ledger/general-ledger.service';
import { JournalEntryAccountService } from '../../entities/journal-entry-account/journal-entry-account.service';
import { CommandBus } from '@nestjs/cqrs';

describe('JournalEntryManagementService', () => {
  let service: JournalEntryManagementService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JournalEntryManagementService,
        {
          provide: JournalEntryService,
          useValue: {},
        },
        {
          provide: GeneralLedgerService,
          useValue: {},
        },
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: JournalEntryAccountService,
          useValue: {},
        },
        {
          provide: CommandBus,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<JournalEntryManagementService>(
      JournalEntryManagementService,
    );
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
