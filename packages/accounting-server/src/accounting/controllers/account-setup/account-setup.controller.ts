import {
  Controller,
  UseGuards,
  Post,
  Req,
  BadRequestException,
} from '@nestjs/common';
import { AccountSetupService } from './account-setup.service';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ACCOUNTANT } from '../../../constants/roles';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { RoleGuard } from '../../../auth/guards/role.guard';

@Controller('account-setup')
export class AccountSetupController {
  constructor(private readonly accountSetupService: AccountSetupService) {}

  @Post('v1/oneTimeSetup')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async accountSetup(@Req() req) {
    if (await this.accountSetupService.oneTimeSetup(req)) {
      return true;
    }
    return BadRequestException;
  }
}
