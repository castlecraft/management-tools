import { Test, TestingModule } from '@nestjs/testing';
import { AccountSetupController } from './account-setup.controller';
import { AccountSetupService } from './account-setup.service';
import { AuthServerVerificationGuard } from '../../../auth/guards/authserver-verification.guard';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { SettingsService } from '../../../system-settings/settings/settings.service';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { HttpService } from '@nestjs/common';

describe('AccountSetup Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [AccountSetupController],
      providers: [
        {
          provide: AccountSetupService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: TokenCacheService,
          useValue: {},
        },
        {
          provide: HttpService,
          useFactory: (...args) => jest.fn(),
        },
      ],
    })
      .overrideGuard(AuthServerVerificationGuard)
      .useValue({})
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();
  });
  it('should be defined', () => {
    const controller: AccountSetupController = module.get<
      AccountSetupController
    >(AccountSetupController);
    expect(controller).toBeDefined();
  });
});
