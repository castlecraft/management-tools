import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFile,
  BadRequestException,
  Req,
  UseGuards,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { BulkCsvGateway } from './event/bulk-csv.gateway';
import { CommandBus } from '@nestjs/cqrs';
import { BulkCsvAccountsCommand } from '../../commands/bulk-accounts-csv/bulk-accounts-csv.command';
import { BulkCsvService } from './bulk-csv.service';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ACCOUNTANT } from '../../../constants/roles';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { RoleGuard } from '../../../auth/guards/role.guard';

@Controller('bulk-csv')
export class BulkCsvController {
  constructor(
    private bulkcsv: BulkCsvGateway,
    private commandBus: CommandBus,
    private bulkCsvService: BulkCsvService,
  ) {}

  @Post('upload')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  @UseInterceptors(FileInterceptor('file'))
  getHello(@UploadedFile('file') file, @Req() req) {
    if (!file)
      throw new BadRequestException(
        'oops!!, may be u missed your csv file ,try again.',
      );
    return this.commandBus.execute(new BulkCsvAccountsCommand(file, req));
  }

  @Post('account')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  @UseInterceptors(FileInterceptor('file'))
  async bulkCsvUpload(@UploadedFile('file') file, @Req() req) {
    if (!file)
      throw new BadRequestException(
        'oops!!, may be u missed your csv file ,try again.',
      );
    this.bulkcsv.handleMessage('File has been uploaded');
    return this.bulkCsvService.bulkAccounts(file, req);
  }
}
