import * as fs from 'fs';
import * as csv from 'csv-parse';
import { fromStream } from './rxjs-from-stream';
import { Observable } from 'rxjs';

export function parse(file, csvParserOptions) {
  return Observable.create(observer => {
    const parser = csv(csvParserOptions);
    const lines$ = fromStream(fs.createReadStream(file).pipe(parser));
    lines$.subscribe(observer);
    return lines$;
  });
}
