import { Test, TestingModule } from '@nestjs/testing';
import { BulkCsvService } from './bulk-csv.service';
import { BulkCsvGateway } from './event/bulk-csv.gateway';
import { AccountService } from '../../entities/account/account.service';

describe('BulkCsvService', () => {
  let service: BulkCsvService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BulkCsvService,
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: BulkCsvGateway,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<BulkCsvService>(BulkCsvService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
