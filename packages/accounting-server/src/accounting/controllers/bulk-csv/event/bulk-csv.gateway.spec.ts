import { Test, TestingModule } from '@nestjs/testing';
import { BulkCsvGateway } from './bulk-csv.gateway';

describe('BulkCsvGateway', () => {
  let gateway: BulkCsvGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BulkCsvGateway],
    }).compile();

    gateway = module.get<BulkCsvGateway>(BulkCsvGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
