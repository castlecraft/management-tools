import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { GeneralLedgerUpdatedEvent } from '../../events/general-ledger-updated/general-ledger-updated.event';

@Injectable()
export class GeneralLedgerManagerAggregateService extends AggregateRoot {
  constructor() {
    super();
  }
  async updateGeneralLedger(
    generalLedgerPayload,
    transactionDate,
    journalEntryTransactionId,
  ) {
    return this.apply(
      new GeneralLedgerUpdatedEvent(
        generalLedgerPayload,
        transactionDate,
        journalEntryTransactionId,
      ),
    );
  }
}
