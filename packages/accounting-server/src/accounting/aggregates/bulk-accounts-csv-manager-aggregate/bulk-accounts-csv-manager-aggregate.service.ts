import { AggregateRoot } from '@nestjs/cqrs';
import { BulkAccountsCsvValidationPolicyService } from '../../policies';
import { Injectable } from '@nestjs/common';
import { BulkCsvAccountsInsertedEvent } from '../../events/bulk-csv-accounts-inserted/bulk-csv-accounts-inserted.event';

import { map } from 'rxjs/operators';

@Injectable()
export class BulkCsvAccountsManagerAggregateService extends AggregateRoot {
  constructor(
    private readonly BulkAccountsCsvValidation: BulkAccountsCsvValidationPolicyService,
  ) {
    super();
  }

  bulkCsvForAccounts(csvFileData, clientHttpRequest) {
    return this.BulkAccountsCsvValidation.bulkAccounts(csvFileData).pipe(
      map((validatedResponse: any) => {
        if (validatedResponse) {
          this.apply(
            new BulkCsvAccountsInsertedEvent(
              validatedResponse,
              clientHttpRequest,
            ),
          );
          return ['200'];
        }
      }),
    );
  }
}
