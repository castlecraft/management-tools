import { Injectable, BadRequestException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { JournalEntryValidationPolicy } from '../../../accounting/policies/journal-entry-validation-policy/journal-entry-validation-policy.service';
import { JournalEntryAccountCreatedEvent } from '../../events/journal-entry-account-created/journal-entry-account-created.event';
import { GenerateMetaDataPolicy } from '../../policies/generate-meta-data/generate-meta-data.service-policy';

@Injectable()
export class JournalEntryManagerAggregateService extends AggregateRoot {
  constructor(
    private readonly journalEntryValidationPolicy: JournalEntryValidationPolicy,
    private readonly generateMetaData: GenerateMetaDataPolicy,
  ) {
    super();
  }

  async newJournalEntryAccount(journalEntryAccountPayload, clientHttpRequest) {
    try {
      const data = await this.journalEntryValidationPolicy.validationPromise(
        journalEntryAccountPayload,
      );
      if (data === true) {
        const journalEntryMetaData = await this.generateMetaData.GenerateJournalEntryMetaData();
        return await this.apply(
          new JournalEntryAccountCreatedEvent(
            journalEntryAccountPayload,
            clientHttpRequest,
            journalEntryMetaData,
          ),
        );
      } else {
        throw new BadRequestException(data);
      }
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
