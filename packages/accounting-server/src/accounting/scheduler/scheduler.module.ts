import { Module, Global } from '@nestjs/common';
import { schedulerProviders } from './scheduler-providers';
import { ConfigModule } from '../../config/config.module';

@Global()
@Module({
  imports: [ConfigModule],
  providers: [...schedulerProviders],
  exports: [...schedulerProviders],
})
export class SchedulerModule {}
