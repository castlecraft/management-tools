import { Provider } from '@nestjs/common';
import { PeriodClosingService } from './period-closing.service';

class MockPeriodClosingService {}

export const schedulerProviders: Provider[] = [
  {
    provide: PeriodClosingService,
    useClass: ['test', 'test-e2e'].includes(process.env.NODE_ENV)
      ? MockPeriodClosingService
      : PeriodClosingService,
  },
];
