import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountNameModifiedEvent } from './account-name-modified.event';
import { AccountService } from '../../../accounting/entities/account/account.service';
import { BulkCsvGateway } from '../../controllers/bulk-csv/event/bulk-csv.gateway';

@EventsHandler(AccountNameModifiedEvent)
export class AccountNameModifiedHandler
  implements IEventHandler<AccountNameModifiedEvent> {
  constructor(
    private readonly accountService: AccountService,
    private readonly socket: BulkCsvGateway,
  ) {}
  async handle(event: AccountNameModifiedEvent) {
    this.socket.handleMessage('validation completed for ' + event.accountName);
    await this.accountService.getRepository().query(
      `UPDATE account
      SET "accountName" = $1 , "modifiedBy"= $2, "modified" = $3
      WHERE "uuid" = $4`,
      [event.accountName, event.modifiedBy, 'NOW()', event.uuid],
    );
    await this.socket.handleMessage(
      'Account Name successfully updated to ' + event.accountName,
    );
  }
}
