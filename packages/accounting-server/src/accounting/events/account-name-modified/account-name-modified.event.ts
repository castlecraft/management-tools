import { IEvent } from '@nestjs/cqrs';

export class AccountNameModifiedEvent implements IEvent {
  constructor(
    public readonly accountName: string,
    public readonly modifiedBy: string,
    public readonly uuid: string,
  ) {}
}
