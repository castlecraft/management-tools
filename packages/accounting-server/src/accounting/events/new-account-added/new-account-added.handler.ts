import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BulkCsvGateway } from '../../controllers/bulk-csv/event/bulk-csv.gateway';
import { NewAccountAddedEvent } from './new-account-added.event';
import { AccountService } from '../../../accounting/entities/account/account.service';
import * as uuidV4 from 'uuid/v4';

@EventsHandler(NewAccountAddedEvent)
export class NewAccountAddedHandler
  implements IEventHandler<NewAccountAddedEvent> {
  constructor(
    private readonly socket: BulkCsvGateway,
    private readonly accountService: AccountService,
  ) {}
  async handle(event: NewAccountAddedEvent) {
    this.socket.handleMessage('validation complete');

    const payload: any = event.payload;
    const uuid = uuidV4();
    const isRoot = false;
    const req: any = event.req;
    const createdBy = req.token.sub;

    const parentId = await this.accountService.getRepository().query(
      `
    SELECT id FROM account WHERE "accountNumber" = $1
    `,
      [payload.parent],
    );

    await this.accountService.getRepository().query(
      `INSERT INTO "account"
        ("uuid", "accountNumber", "accountName", "accountType","isRoot", "isGroup", "createdBy", "modifiedBy", "modified", "parentId")
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
      [
        uuid,
        payload.accountNumber,
        payload.accountName,
        payload.accountType,
        isRoot,
        payload.isGroup,
        createdBy,
        createdBy,
        'NOW()',
        parentId[0].id,
      ],
    );

    const child = await this.accountService
      .getRepository()
      .query(`select id from account where uuid = $1 `, [uuid]);
    await this.accountService
      .getRepository()
      .query(
        `INSERT into "account_closure" ("id_ancestor" , "id_descendant") VALUES ($1, $2)`,
        [child[0].id, parentId[0].id],
      );

    this.socket.handleMessage('Account Successfully Created');
  }
}
