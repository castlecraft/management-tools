import { IEvent } from '@nestjs/cqrs';
import { Account } from '../../entities/account/account.entity';

export class GeneralLedgerUpdatedEvent implements IEvent {
  constructor(
    public readonly generalLedgerPayload: { accounts: Account[] },
    public readonly transactionDate: Date,
    public readonly journalEntryTransactionId: string,
  ) {}
}
