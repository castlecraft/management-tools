import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BulkCsvGateway } from '../../controllers/bulk-csv/event/bulk-csv.gateway';
import { JournalEntryAccountCreatedEvent } from './journal-entry-account-created.event';
import { AccountService } from '../../entities/account/account.service';
import { BULK_ENTRY_LIMIT } from '../../../constants/filesystem';

@EventsHandler(JournalEntryAccountCreatedEvent)
export class JournalEntryAccountCreatedHandler
  implements IEventHandler<JournalEntryAccountCreatedEvent> {
  constructor(
    private readonly socket: BulkCsvGateway,
    private readonly accountService: AccountService,
  ) {}
  async handle(event: JournalEntryAccountCreatedEvent) {
    await this.accountService.getRepository().query(
      `
    INSERT INTO journal_entry VALUES($1,$2,$3)
    `,
      [event.journalEntryMetaData.uuid, event.journalEntryMetaData.date, true],
    );

    this.socket.handleMessage('JE successfully validated');

    const clientHttpRequest: any = event.clientHttpRequest;
    const journalEntryPayload = event.journalEntryPayload;
    const createdBy = clientHttpRequest.token.sub;

    let batchLength = 0;
    const data = journalEntryPayload.accounts;
    while (batchLength < data.length) {
      let generalLedgerQuery =
        'INSERT INTO general_ledger ( "transactionDate", "amountType","amount", "journalEntryTransactionId","accountId") VALUES';
      let journalEntryQuery =
        'insert into journal_entry_account ( "entryType","amount", "createdBy", "journalEntryTransactionId","accountId") VALUES';
      const batch = data.slice(batchLength, batchLength + BULK_ENTRY_LIMIT);

      const journalEntryMetaData = event.journalEntryMetaData;
      const journalEntryParams = [];
      const generalLedgerParams = [];
      let account;
      let iterator = 1;
      for (account of batch) {
        const accountId = await this.accountService.getRepository().query(
          `
      SELECT id FROM account WhERE "accountNumber" = $1
      `,
          [account.account],
        );
        journalEntryParams.push(
          account.entryType,
          account.amount,
          createdBy,
          journalEntryMetaData.uuid,
          accountId[0].id,
        );

        generalLedgerParams.push(
          journalEntryMetaData.date,
          account.entryType,
          account.amount,
          journalEntryMetaData.uuid,
          accountId[0].id,
        );
        const sQuery =
          ' ( $' +
          iterator +
          ', $' +
          (iterator + 1) +
          ', $' +
          (iterator + 2) +
          ', $' +
          (iterator + 3) +
          ', $' +
          (iterator + 4) +
          '),';
        iterator = iterator + 5;
        journalEntryQuery += sQuery;
        generalLedgerQuery += sQuery;
      }
      journalEntryQuery = journalEntryQuery.slice(0, -1) + ';';
      generalLedgerQuery = generalLedgerQuery.slice(0, -1) + ';';

      await this.accountService
        .getRepository()
        .query(journalEntryQuery, journalEntryParams);
      await this.accountService
        .getRepository()
        .query(generalLedgerQuery, generalLedgerParams);
      batchLength += BULK_ENTRY_LIMIT;
    }
    this.socket.handleMessage('All JE successfully Created');
  }
}
