import { IEvent } from '@nestjs/cqrs';
import { Account } from '../../entities/account/account.entity';

export class JournalEntryAccountCreatedEvent implements IEvent {
  constructor(
    public readonly journalEntryPayload: { accounts: Account[] },
    public readonly clientHttpRequest: any[],
    public readonly journalEntryMetaData: {
      uuid: string;
      date: Date;
    },
  ) {}
}
