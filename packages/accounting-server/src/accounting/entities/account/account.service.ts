import { Injectable, BadRequestException } from '@nestjs/common';
import { Account } from './account.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { TreeRepository } from 'typeorm';
import { ACCOUNT_TYPES } from '../../../constants/app-strings';
import * as uuidv4 from 'uuid/v4';
import { CommandBus } from '@nestjs/cqrs';
import { ModifyAccountNameCommand } from '../../commands/modify-account-name/modify-account-name.command';
import { AddNewAccountCommand } from '../../../accounting/commands/add-new-account/add-new-account.command';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { INVALID_ACCOUNT_NUMBER } from '../../../constants/messages';
import { POSTGRES_CONNECTION_NAME } from '../../../constants/postgres.connection';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(Account, POSTGRES_CONNECTION_NAME)
    private readonly accountRepository: TreeRepository<Account>,
    private readonly commandBus: CommandBus,
  ) {}

  async find(limit, offset, search, sort, query) {
    let account;
    if (!search) {
      account = await this.accountRepository
        .createQueryBuilder('account') // first argument is an alias. Alias is what you are selecting - photos. You must specify it.
        .orderBy(`"accountName"`, sort)
        .where(query)
        .skip(offset)
        .take(limit)
        .getMany();
      const out = {
        docs: account,
        offset,
        length: await this.accountRepository.count(),
      };

      return out;
    } else {
      search = search;
      account = await this.accountRepository
        .createQueryBuilder('account') // first argument is an alias. Alias is what you are selecting - photos. You must specify it.
        .orderBy(`"accountName"`, sort)
        .where('"accountName" LIKE :search', { search: `%${search}%` })
        .orWhere('"accountNumber" LIKE :search', { search: `%${search}%` })
        .orWhere('"accountType" LIKE :search', { search: `%${search}%` })
        .skip(offset)
        .take(limit)
        .getMany();
      const out = {
        docs: account,
        offset,
        length: await this.accountRepository.count(),
      };

      return out;
    }
  }

  async create(payload, req, isBulk) {
    let valid = isBulk;
    const createdBy = req.token.sub;
    const isRoot = false;
    const parent = await this.findParent(payload.parent);
    if (!valid) {
      valid = this.validateAccount(
        payload.accountType,
        payload.parent,
        payload.accountNumber,
      );
      payload.uuid = uuidv4();
    } else if (!valid) {
      throw new BadRequestException();
    }

    await this.accountRepository.query(
      `INSERT INTO "account"
        ("uuid", "accountNumber", "accountName", "accountType","isRoot", "isGroup", "createdBy", "modifiedBy", "modified", "parentId")
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
      [
        payload.uuid,
        payload.accountNumber,
        payload.accountName,
        payload.accountType,
        isRoot,
        payload.isGroup,
        createdBy,
        createdBy,
        'NOW()',
        parent[0].id,
      ],
    );
    await this.createAccountClosure(parent[0].id, payload.uuid);
    return payload;
  }

  async findOne(params): Promise<any> {
    if (params.uuid) {
      return await this.findAccountByUUID(params.uuid);
    } else {
      return await this.accountRepository.findOne(params);
    }
  }

  async findAccountByUUID(uuid) {
    const account = await this.accountRepository.findOne({ uuid });
    if (account.isRoot === true) {
      return account;
    }
    const parentId = await this.accountRepository.query(
      'SELECT "parentId" FROM account WHERE uuid = $1',
      [uuid],
    );

    const data = parentId.length > 0 ? parentId[0].parentId : null;
    if (!data) throw new BadRequestException();

    const parent = await this.accountRepository.query(
      'SELECT "accountNumber" FROM account WHERE id = $1',
      [data],
    );

    account.parent = parent.length > 0 ? parent[0].accountNumber : null;
    if (!account.parent) throw new BadRequestException();
    return account;
  }

  async findParent(parent) {
    return await this.accountRepository.query(
      'select id from account where "accountNumber" = $1',
      [parent],
    );
  }

  async update(payload, req) {
    return await this.commandBus.execute(
      new ModifyAccountNameCommand(
        payload.accountName,
        payload.uuid,
        req.token.sub,
      ),
    );
  }

  async validateAccount(
    payloadAccountType,
    payloadAccountParent,
    accountNumber,
  ) {
    const parent = await this.validateParent(
      payloadAccountParent,
      accountNumber,
    );

    if (ACCOUNT_TYPES.includes(payloadAccountType)) {
      if (parent) return true;
    }
    return false;
  }

  validateParentObservable(parent, accountNumber) {
    return from(
      this.getRepository().query(
        `
    SELECT COUNT(id) FROM account WHERE "accountNumber" = $1 and "isGroup" = true or
    "accountNumber" in  (select "accountNumber" from account where "accountNumber" = $2);
    `,
        [parent, accountNumber],
      ),
    ).pipe(
      map(response => {
        if (Number(response[0].count) === 1) {
          return true;
        } else return false;
      }),
    );
  }

  validateAccountObservable(
    payloadAccountType,
    payloadAccountParent,
    accountNumber,
  ) {
    return this.validateParentObservable(
      payloadAccountParent,
      accountNumber,
    ).pipe(
      map(response => {
        if (ACCOUNT_TYPES.includes(payloadAccountType)) {
          if (response) return true;
        }
        return false;
      }),
    );
  }

  async createAccountClosure(parent, uuid) {
    const child = await this.accountRepository.query(
      `select id from account where uuid = $1 `,
      [uuid],
    );
    await this.accountRepository.query(
      `INSERT into "account_closure" ("id_ancestor" , "id_descendant") VALUES ($1, $2)`,
      [child[0].id, parent],
    );
  }

  async getLastClosing(id) {
    return await this.getRepository().query(
      `
      SELECT "closingBalance"
      FROM account_statement WHERE "lastUpdated" IN
      (SELECT MAX("lastUpdated") FROM account_statement WHERE "accountId" = $1 )
      `,
      [id],
    );
  }
  async validateParent(parent, accountNumber) {
    const validParent = await this.getRepository().query(
      `
    SELECT COUNT(id) FROM account WHERE "accountNumber" = $1 and "isGroup" = true or
    "accountNumber" in  (select "accountNumber" from account where "accountNumber" = $2);
    `,
      [parent, accountNumber],
    );
    if (Number(validParent[0].count) === 1) {
      return true;
    } else return false;
  }

  async getAccountId(accountNumber?) {
    if (accountNumber) {
      return await this.getRepository().query(
        `
      SELECT id,"parentId" FROM account WHERE "accountNumber" = $1
      `,
        [accountNumber],
      );
    }
    return await this.getRepository().query(`
    SELECT id FROM account where "isGroup" = 'false'
    `);
  }

  getRepository() {
    return this.accountRepository;
  }

  async addNewAccount(payload, req) {
    return await this.commandBus.execute(
      new AddNewAccountCommand(payload, req),
    );
  }

  async validateAccountForJE(accountNumber) {
    const account = await this.accountRepository.query(
      `
    SELECT * FROM account WHERE "accountNumber" = $1 and "isGroup" = false
    `,
      [accountNumber],
    );
    if (account.length > 0) {
      return account[0];
    } else {
      throw new BadRequestException(INVALID_ACCOUNT_NUMBER);
    }
  }
}
