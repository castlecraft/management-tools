import { Test, TestingModule } from '@nestjs/testing';
import { GeneralLedgerArchiveService } from './general-ledger-archive.service';

describe('GeneralLedgerArchiveService', () => {
  let service: GeneralLedgerArchiveService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GeneralLedgerArchiveService],
    }).compile();
    service = module.get<GeneralLedgerArchiveService>(
      GeneralLedgerArchiveService,
    );
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
