import {
  Entity,
  CreateDateColumn,
  OneToMany,
  BaseEntity,
  Column,
  PrimaryColumn,
} from 'typeorm';
import { JournalEntryAccount } from '../journal-entry-account/journal-entry-account.entity';
import { GeneralLedger } from '../general-ledger/general-ledger.entity';

@Entity()
export class JournalEntry extends BaseEntity {
  @PrimaryColumn()
  transactionId: string;

  @CreateDateColumn()
  transactionDate: Date;

  @Column({ type: 'boolean' })
  isLocked: boolean = true;

  @OneToMany(type => JournalEntryAccount, account => account.journalEntry, {
    cascade: true,
  })
  accounts: JournalEntryAccount[];

  @OneToMany(
    type => GeneralLedger,
    generalLedger => generalLedger.journalEntry,
    {
      cascade: true,
    },
  )
  generalLedgerJournalEntries: GeneralLedger[];
}
