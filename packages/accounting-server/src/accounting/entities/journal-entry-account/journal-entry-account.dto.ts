import { TransactionType } from '../general-ledger/general-ledger.entity';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { Account } from '../account/account.entity';

export class JournalEntryAccountDto {
  @IsNotEmpty()
  account: Account;

  @IsNotEmpty()
  entryType: TransactionType;

  @IsNumber()
  amount: number;
}
