import { Test, TestingModule } from '@nestjs/testing';
import { JournalEntryAccountService } from './journal-entry-account.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { JournalEntryAccount } from './journal-entry-account.entity';
import { POSTGRES_CONNECTION_NAME } from '../../../constants/postgres.connection';

describe('JournalEntryAccountService', () => {
  let service: JournalEntryAccountService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JournalEntryAccountService,
        {
          provide: getRepositoryToken(
            JournalEntryAccount,
            POSTGRES_CONNECTION_NAME,
          ),
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<JournalEntryAccountService>(
      JournalEntryAccountService,
    );
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
