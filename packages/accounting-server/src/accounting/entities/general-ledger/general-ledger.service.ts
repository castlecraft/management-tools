import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { GeneralLedger } from './general-ledger.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { POSTGRES_CONNECTION_NAME } from '../../../constants/postgres.connection';

@Injectable()
export class GeneralLedgerService {
  constructor(
    @InjectRepository(GeneralLedger, POSTGRES_CONNECTION_NAME)
    private readonly repository: Repository<GeneralLedger>,
  ) {}

  async updateGL(payload, id) {
    let account;
    let i = 1;
    const generalLedgerParams = [];
    let generalLedgerQuery =
      'insert into general_ledger ( "transactionDate", "amountType","amount", "journalEntryTransactionId","accountId") VALUES';

    for (account of payload.accounts) {
      const index = await this.getRepository().query(
        `
      SELECT id FROM account WhERE "accountNumber" = $1
      `,
        [account.account],
      );
      generalLedgerParams.push(
        id.transactionDate,
        account.entryType,
        account.amount,
        id.transactionId,
        index[0].id,
      );
      const sQuery =
        ' ( $' +
        i +
        ', $' +
        (i + 1) +
        ', $' +
        (i + 2) +
        ', $' +
        (i + 3) +
        ', $' +
        (i + 4) +
        '),';
      i = i + 5;
      generalLedgerQuery += sQuery;
    }
    generalLedgerQuery = generalLedgerQuery.slice(0, -1) + ';';
    this.getRepository().query(generalLedgerQuery, generalLedgerParams);
  }
  getRepository() {
    return this.repository;
  }
}
