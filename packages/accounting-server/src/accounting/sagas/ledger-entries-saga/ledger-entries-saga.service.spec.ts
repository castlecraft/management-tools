import { Test, TestingModule } from '@nestjs/testing';
import { LedgerEntriesSaga } from './ledger-entries-saga.service';

describe('LedgerEntriesSaga', () => {
  let service: LedgerEntriesSaga;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LedgerEntriesSaga],
    }).compile();

    service = module.get<LedgerEntriesSaga>(LedgerEntriesSaga);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
