import { Injectable, BadRequestException } from '@nestjs/common';
import * as uuidv4 from 'uuid/v4';
import { of, from, throwError } from 'rxjs';
import { mergeMap, switchMap, concatAll, map, toArray } from 'rxjs/operators';
import { validate } from 'class-validator';
import { BulkCsvGateway } from '../../controllers/bulk-csv/event/bulk-csv.gateway';
import * as fs from 'fs';
import { AccountDto } from '../../controllers/account/account-dto';
import * as csv from '../../controllers/bulk-csv/csv-utils/rxjs-csv';
import { AccountService } from '../../entities/account/account.service';

@Injectable()
export class BulkAccountsCsvValidationPolicyService {
  constructor(
    private readonly socketGateway: BulkCsvGateway,
    private readonly accountService: AccountService,
  ) {}

  accountDto = new AccountDto();

  bulkAccounts(file) {
    return this.validateCsvData(file).pipe(
      mergeMap((res: any) => {
        return this.accountService
          .validateAccountObservable(
            res.accountType,
            res.parent,
            res.accountNumber,
          )
          .pipe(
            mergeMap((valid: any) => {
              if (!valid) {
                this.socketGateway.handleMessage(
                  'error in CSV file at Account Number- ' +
                    res.accountNumber +
                    ' parent- ' +
                    res.parent,
                );
                throw new BadRequestException(
                  'Invalid account entry at - Account Number ' +
                    res.accountNumber +
                    ' or Parent Id ' +
                    res.parent,
                );
              } else {
                this.socketGateway.handleMessage(
                  'CSV validated , Inserting Data Entry',
                );
                return of(res);
              }
            }),
          );
      }),
      toArray(),
    );
  }

  validateCsvData(file) {
    return this.csvToJson(file).pipe(
      mergeMap((res: any) => {
        return from(res).pipe(
          switchMap((account: any) => {
            if (!account) {
              this.socketGateway.handleMessage('Invalid Csv Account');
              return throwError(new BadRequestException('Invalid Csv Account'));
            }
            account.uuid = uuidv4();
            account.isGroup.toLowerCase();
            if (account.isGroup === 'true') {
              account.isGroup = true;
            }
            if (account.isGroup === 'false') {
              account.isGroup = false;
            }

            Object.assign(this.accountDto, account);
            return from(
              validate(this.accountDto, {
                forbidUnknownValues: true,
                forbidNonWhitelisted: true,
                whitelist: true,
              }),
            );
          }),
          switchMap(validation => {
            if (validation.length === 0) {
              this.socketGateway.handleMessage('CSV Successfully validated');
              return of(res);
            } else {
              return throwError(new BadRequestException(validation));
            }
          }),
        );
      }),
      concatAll(),
    );
  }

  csvToJson(file) {
    const uuid = uuidv4();
    const path = '/tmp/' + uuid + '.csv',
      buffer = Buffer.from(file.buffer.toString());

    fs.open(path, 'w', (err, fd) => {
      if (err) {
        throw new BadRequestException('error opening file: ' + err);
      }
      fs.write(fd, buffer, 0, buffer.length, null, error => {
        if (error)
          throw new BadRequestException('error writing file: ' + error);
        fs.close(fd, () => {});
      });
    });
    return csv.parse(path, { columns: true }).pipe(
      map(res => {
        fs.unlink(path, error => {});
        return res;
      }),
      toArray(),
    );
  }
}
