import { Test, TestingModule } from '@nestjs/testing';
import { JournalEntryValidationPolicy } from './journal-entry-validation-policy.service';
import { AccountService } from '../../entities/account/account.service';

describe('JournalEntryValidationPolicyService', () => {
  let service: JournalEntryValidationPolicy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JournalEntryValidationPolicy,
        {
          provide: AccountService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<JournalEntryValidationPolicy>(
      JournalEntryValidationPolicy,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
