import { Injectable, BadRequestException } from '@nestjs/common';
import { AccountService } from '../../../accounting/entities/account/account.service';
import {
  INVALID_PARENT_ACCOUNT_NUMBER,
  INVALID_ACCOUNT_NUMBER,
} from '../../../constants/messages';

@Injectable()
export class AccountValidationPolicy {
  constructor(private readonly accountService: AccountService) {}

  async validateParentAccountNumber(parentAccountNumber) {
    const parent = await this.accountService
      .getRepository()
      .query('select id from account where "accountNumber" = $1', [
        parentAccountNumber,
      ]);
    if (parent) {
      return new BadRequestException(INVALID_PARENT_ACCOUNT_NUMBER);
    } else {
      return false;
    }
  }

  async validateAccountNumber(accountNumber) {
    const validParent = await this.accountService.getRepository().query(
      `
    SELECT COUNT(id) FROM account WHERE "accountNumber" = $1;
    `,
      [accountNumber],
    );
    if (Number(validParent[0].count) > 0) {
      return new BadRequestException(INVALID_ACCOUNT_NUMBER);
    }
  }
}
