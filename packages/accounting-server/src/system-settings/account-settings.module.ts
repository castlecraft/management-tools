import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingsService } from './settings/settings.service';
import { Settings } from './settings/settings.collection';
import { MONGODB_CONNECTION_NAME } from '../constants/monodb.connection';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([Settings], MONGODB_CONNECTION_NAME)],
  providers: [SettingsService],
  exports: [SettingsService],
})
export class AccountSettingsModule {}
