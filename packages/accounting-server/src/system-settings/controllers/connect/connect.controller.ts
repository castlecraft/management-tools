import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { AuthServerVerificationGuard } from '../../../auth/guards/authserver-verification.guard';
import { USER_ALREADY_EXIST } from '../../../constants/messages';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { RoleGuard } from '../../../auth/guards/role.guard';

@Controller('connect')
@UseGuards(TokenGuard, RoleGuard)
export class ConnectController {
  constructor(private readonly tokenCacheService: TokenCacheService) {}

  @Post('v1/token_delete')
  @UseGuards(AuthServerVerificationGuard)
  async tokenDelete(@Body('accessToken') accessToken) {
    await this.tokenCacheService.deleteMany({ accessToken });
  }

  @Post('v1/user_delete')
  @UseGuards(AuthServerVerificationGuard)
  async userDelete(@Body('user') user) {
    throw USER_ALREADY_EXIST;
  }
}
