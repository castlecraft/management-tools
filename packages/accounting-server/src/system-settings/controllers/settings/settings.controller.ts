import {
  Controller,
  Get,
  UseGuards,
  Post,
  Body,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { SettingsService } from '../../../system-settings/settings/settings.service';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { PeriodClosingDTO } from './period-closing-dto';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { ACCOUNTANT } from '../../../constants/roles';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { SettingsDto } from '../../settings/settings.dto';

@Controller('settings')
export class SettingsController {
  constructor(private readonly settingsService: SettingsService) {}

  @Get('v1/getSettings')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async getSettings() {
    return await this.settingsService.findAll();
  }

  @Post('v1/periodClosing')
  @UsePipes(ValidationPipe)
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async setPeriodClosing(@Body('body') periodClosing: PeriodClosingDTO) {
    return await this.settingsService.setPeriodClosing(
      periodClosing.periodClosing,
    );
  }

  @Put('v1/update')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async updateSettings(@Body('settings') settings, @Query('query') query) {
    return await this.settingsService.update(query, settings);
  }

  @Post('v1/create')
  @UseGuards(TokenGuard, RoleGuard)
  @Roles(ACCOUNTANT)
  async createSettings(@Body('settings') settings: SettingsDto) {
    return await this.settingsService.save(settings);
  }
}
