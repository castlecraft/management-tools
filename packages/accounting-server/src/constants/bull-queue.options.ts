export interface BullOptions {
  redis: {
    host: string;
    port: number;
    password?: string;
  };
}
