import { ConfigService } from '../config/config.service';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import { Account } from '../accounting/entities/account/account.entity';
import { AccountStatement } from '../accounting/entities/account-statement/account-statement.entity';
import { GeneralLedger } from '../accounting/entities/general-ledger/general-ledger.entity';
import { GeneralLedgerArchive } from '../accounting/entities/general-ledger-archive/general-ledger-archive.entity';
import { JournalEntry } from '../accounting/entities/journal-entry/journal-entry.entity';
import { JournalEntryAccount } from '../accounting/entities/journal-entry-account/journal-entry-account.entity';

const config = new ConfigService();

export const POSTGRES_CONNECTION_NAME = 'postgres';

export const POSTGRES_CONNECTION: PostgresConnectionOptions = {
  name: POSTGRES_CONNECTION_NAME,
  type: 'postgres',
  username: config.get('POSTGRESDB_USER'),
  password: config.get('POSTGRESDB_PASSWORD'),
  host: config.get('POSTGRESDB_HOST'),
  database: config.get('POSTGRESDB_NAME'),
  logging: true,
  entities: [
    Account,
    AccountStatement,
    GeneralLedger,
    GeneralLedgerArchive,
    JournalEntry,
    JournalEntryAccount,
  ],
};
