import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { HttpModule, Module } from '@nestjs/common';
import { CommonModule } from './common/common.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { POSTGRES_CONNECTION } from './constants/postgres.connection';
import { MONGODB_CONNECTION } from './constants/monodb.connection';
import { SchedulerModule } from './accounting/scheduler/scheduler.module';
import { AccountingModule } from './accounting/accounting.module';
import { AccountSettingsModule } from './system-settings/account-settings.module';
import { AppController } from './app.controller';
import { SetupController } from './system-settings/controllers/setup/setup.controller';
import { ConnectController } from './system-settings/controllers/connect/connect.controller';
import { SettingsController } from './system-settings/controllers/settings/settings.controller';
import { AppService } from './app.service';
import { SetupService } from './system-settings/controllers/setup/setup.service';
import { AuthServerVerificationGuard } from './auth/guards/authserver-verification.guard';
import { TokenGuard } from './auth/guards/token.guard';
import { RoleGuard } from './auth/guards/role.guard';
import { AccountSetupService } from './accounting/controllers/account-setup/account-setup.service';
import { AccountReportsService } from './accounting/controllers/account-reports/account-reports.service';
import { TreeService } from './accounting/controllers/tree/tree.service';
import { BulkCsvService } from './accounting/controllers/bulk-csv/bulk-csv.service';
import { BulkCsvGateway } from './accounting/controllers/bulk-csv/event/bulk-csv.gateway';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    CommonModule,
    TypeOrmModule.forRoot(POSTGRES_CONNECTION),
    TypeOrmModule.forRoot(MONGODB_CONNECTION),
    SchedulerModule,
    AccountingModule,
    AccountSettingsModule,
    AuthModule,
  ],
  controllers: [
    AppController,
    SetupController,
    ConnectController,
    SettingsController,
  ],
  providers: [
    AppService,
    SetupService,
    AuthServerVerificationGuard,
    TokenGuard,
    RoleGuard,
    AccountSetupService,
    AccountReportsService,
    TreeService,
    BulkCsvService,
    BulkCsvGateway,
  ],
})
export class AppModule {}
