import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TokenCacheService } from './entities/token-cache/token-cache.service';
import { TokenCache } from './entities/token-cache/token-cache.collection';
import { MONGODB_CONNECTION_NAME } from '../constants/monodb.connection';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([TokenCache], MONGODB_CONNECTION_NAME)],
  providers: [TokenCacheService],
  exports: [TokenCacheService],
})
export class AuthModule {}
