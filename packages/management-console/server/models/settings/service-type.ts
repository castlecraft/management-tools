export const ACCOUNTING = 'accounting';
export const MANAGEMENT_CONSOLE = 'management-console';
export const AUTHORIZATION_SERVER = 'authorization-server';
export const COMMUNICATION_SERVER = 'communication-server';
export const IDENTITY_PROVIDER = 'identity-provider';
export const INFRASTRUCTURE_CONSOLE = 'infrastructure-console';

export const SERVICES = [
  ACCOUNTING,
  MANAGEMENT_CONSOLE,
  AUTHORIZATION_SERVER,
  COMMUNICATION_SERVER,
  IDENTITY_PROVIDER,
  INFRASTRUCTURE_CONSOLE,
];
