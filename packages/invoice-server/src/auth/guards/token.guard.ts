import {
  CanActivate,
  ExecutionContext,
  Injectable,
  NotImplementedException,
  HttpService,
  UnauthorizedException,
} from '@nestjs/common';
import { switchMap, retry } from 'rxjs/operators';
import { of, from, throwError } from 'rxjs';
import * as Express from 'express';
import { SettingsService } from '../../system-settings/settings/settings.service';
import { TokenCacheService } from '../../auth/entities/token-cache/token-cache.service';
import { TOKEN } from '../../constants/app-strings';

@Injectable()
export class TokenGuard implements CanActivate {
  constructor(
    private readonly settingsService: SettingsService,
    private readonly tokenCacheService: TokenCacheService,
    private readonly http: HttpService,
  ) {}
  canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req = httpContext.getRequest();
    const accessToken = this.getAccessToken(req);
    if (!accessToken) {
      return throwError(new UnauthorizedException());
    }
    return from(this.tokenCacheService.findOne({ accessToken })).pipe(
      switchMap(cachedToken => {
        if (!cachedToken) {
          return this.introspectToken(accessToken, req);
        } else if (new Date().getTime() < cachedToken.exp) {
          req[TOKEN] = cachedToken;
          return of(true);
        } else {
          from(
            this.tokenCacheService.deleteMany({
              accessToken: cachedToken.accessToken,
            }),
          ).subscribe({
            next: success => {},
            error: error => {
              throw error;
            },
          });
          return this.introspectToken(accessToken, req);
        }
      }),
    );
  }

  introspectToken(accessToken: string, req: Express.Request) {
    return from(this.settingsService.find()).pipe(
      switchMap(settings => {
        if (!settings) {
          throw new NotImplementedException();
        }
        const baseEncodedCred = Buffer.from(
          settings.clientId + ':' + settings.clientSecret,
        ).toString('base64');
        return this.http
          .post(
            settings.introspectionURL,
            { token: accessToken },
            { headers: { Authorization: 'Basic ' + baseEncodedCred } },
          )
          .pipe(
            retry(3),
            switchMap(response => {
              return from(this.cacheToken(response.data, accessToken)).pipe(
                switchMap(cachedToken => {
                  req[TOKEN] = cachedToken;
                  return of(cachedToken.active);
                }),
              );
            }),
          );
      }),
    );
  }

  getAccessToken(request) {
    if (!request.headers.authorization) {
      if (!request.query.access_token) {
        return null;
      }
    }
    return (
      request.query.access_token ||
      request.headers.authorization.split(' ')[1] ||
      null
    );
  }

  cacheToken(introspectedToken: any, accessToken: string) {
    introspectedToken.accessToken = accessToken;
    introspectedToken.clientId = introspectedToken.client_id;
    return this.tokenCacheService.save(introspectedToken);
  }
}
