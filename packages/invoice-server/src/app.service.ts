import { Injectable } from '@nestjs/common';
import { PLEASE_RUN_SETUP } from './constants/messages';
import { SetupService } from './system-settings/controllers/setup/setup.service';

@Injectable()
export class AppService {
  constructor(private readonly setupService: SetupService) {}

  async info() {
    let info;

    try {
      info = await this.setupService.getInfo();
    } catch (error) {
      info = { message: PLEASE_RUN_SETUP };
    }
    return info;
  }
}
