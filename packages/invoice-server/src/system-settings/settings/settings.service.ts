import { Injectable, BadRequestException } from '@nestjs/common';
import { SETTINGS } from './settings.schema';
import { SettingsInterface } from './settings.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { SETTINGS_ALREADY_EXISTS } from '../../constants/messages';

@Injectable()
export class SettingsService {
  constructor(
    @InjectModel(SETTINGS)
    private readonly settingsModel: Model<SettingsInterface>,
  ) {}

  async save(params) {
    let serverSettings = new this.settingsModel();
    if (params.uuid) {
      const exists: number = await this.count();
      serverSettings = await this.findOne({ uuid: params.uuid });
      serverSettings.appURL = params.appURL;
      if (exists > 0 && !serverSettings) {
        throw new BadRequestException(SETTINGS_ALREADY_EXISTS);
      }
    } else {
      Object.assign(serverSettings, params);
    }
    return await serverSettings.save();
  }
  async find() {
    const settings = await this.settingsModel.find().exec();
    if (!settings.length) {
      throw new BadRequestException('settings Not Found or setup incomplete');
    }
    return settings[0];
  }

  async findAll() {
    return await this.settingsModel.find();
  }

  async findOne(params) {
    return await this.settingsModel.findOne(params);
  }

  async update(query, params) {
    return await this.settingsModel.update(query, params);
  }

  async count() {
    return await this.settingsModel.estimatedDocumentCount();
  }

  getModel() {
    return this.settingsModel;
  }
}
