import { ICommand } from '@nestjs/cqrs';
import { SettingsDto } from '../../settings/settings.dto';

export class SetupAccountingServerCommand implements ICommand {
  constructor(public readonly settingsData: SettingsDto) {}
}
