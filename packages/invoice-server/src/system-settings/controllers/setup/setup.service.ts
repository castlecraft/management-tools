import { Injectable, HttpService, BadRequestException } from '@nestjs/common';
import { SettingsInterface as Settings } from '../../../system-settings/settings/settings.interface';
import { SettingsService } from '../../../system-settings/settings/settings.service';
import {
  SETTINGS_ALREADY_EXISTS,
  SOMETHING_WENT_WRONG,
} from '../../../constants/messages';

@Injectable()
export class SetupService {
  protected settings: Settings;

  constructor(
    protected readonly settingsService: SettingsService,
    protected readonly http: HttpService,
  ) {}

  async setup(params) {
    if (await this.settingsService.count()) {
      throw new BadRequestException(SETTINGS_ALREADY_EXISTS);
    }
    this.http
      .get(params.authServerURL + '/.well-known/openid-configuration')
      .subscribe({
        next: async response => {
          params.authorizationURL = response.data.authorization_endpoint;
          params.tokenURL = response.data.token_endpoint;
          params.profileURL = response.data.userinfo_endpoint;
          params.revocationURL = response.data.revocation_endpoint;
          params.introspectionURL = response.data.introspection_endpoint;
          params.callbackURLs = [
            params.appURL + '/index.html',
            params.appURL + '/silent-refresh.html',
          ];
          this.settings = await this.settingsService.save(params);
          return this.settings;
        },
        error: error => {
          // TODO : meaningful errors
          throw new BadRequestException(SOMETHING_WENT_WRONG);
        },
      });
  }

  async getInfo() {
    const info = await this.settingsService.find();
    if (info) {
      info.clientSecret = undefined;
      info._id = undefined;
    }
    return info;
  }
}
