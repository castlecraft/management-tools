import {
  Post,
  Controller,
  Body,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { SetupService } from './setup.service';
import { SettingsDto } from '../../../system-settings/settings/settings.dto';

@Controller('setup')
export class SetupController {
  constructor(private readonly settingsService: SetupService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async setup(@Body() SettingsDTO: SettingsDto) {
    return await this.settingsService.setup(SettingsDTO);
  }
}
