import { SalesInvoiceManagerAggregate } from './sales-invoice-manager-aggregate/sales-invoice-manager-aggregate';
import { CRUDOperationService } from './query-services/crudoperation/crudoperation.service';

export const SalesInvoiceAggregate = [
  SalesInvoiceManagerAggregate,
  CRUDOperationService,
];
