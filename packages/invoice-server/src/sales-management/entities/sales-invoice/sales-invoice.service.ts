import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { SalesInvoice } from './sales-invoice.interface';
import { SALES_INVOICE } from './sales-invoice.schema';

@Injectable()
export class SalesInvoiceService {
  constructor(
    @InjectModel(SALES_INVOICE)
    private readonly salesInvoiceModel: Model<SalesInvoice>,
  ) {}

  async findAll(): Promise<SalesInvoice[]> {
    return await this.salesInvoiceModel.find().exec();
  }

  async save(params) {
    const createdsalesInvoice = new this.salesInvoiceModel(params);
    return await createdsalesInvoice.save();
  }

  async findOne(param) {
    return await this.salesInvoiceModel.findOne(param);
  }

  getModel() {
    return this.salesInvoiceModel;
  }
}
