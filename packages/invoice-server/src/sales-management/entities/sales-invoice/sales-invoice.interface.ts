import { Document } from 'mongoose';

export interface SalesInvoice extends Document {
  uuid?: string;
  date?: Date;
  createdBy?: string;
  invoiceName?: string;
  customerId?: string;
  customerName?: string;
  invoiceNumber?: string;
  invoiceItems?: any;
  tax?: number;
  termsAndConditions?: string;
}
