import { EventPublisher, ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { UpdateSalesInvoiceCommand } from './update-sales-invoice.command';
import { SalesInvoiceManagerAggregate } from '../../aggregates/sales-invoice-manager-aggregate/sales-invoice-manager-aggregate';

@CommandHandler(UpdateSalesInvoiceCommand)
export class UpdateSalesInvoiceHandler
  implements ICommandHandler<UpdateSalesInvoiceCommand> {
  constructor(
    private readonly manager: SalesInvoiceManagerAggregate,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: UpdateSalesInvoiceCommand) {
    const { SalesInvoiceData: salesInvoiceData, clientHttpRequest } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateSalesInvoice(salesInvoiceData, clientHttpRequest);
    aggregate.commit();
  }
}
