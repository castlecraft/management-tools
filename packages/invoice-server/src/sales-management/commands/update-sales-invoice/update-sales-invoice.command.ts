import { ICommand } from '@nestjs/cqrs';
import { SalesInvoiceDto } from '../../controllers/sales-invoice/sales-invoice-dto';

export class UpdateSalesInvoiceCommand implements ICommand {
  constructor(
    public readonly SalesInvoiceData: SalesInvoiceDto,
    public readonly clientHttpRequest: any,
  ) {}
}
