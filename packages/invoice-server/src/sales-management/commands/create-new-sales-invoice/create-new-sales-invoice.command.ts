import { ICommand } from '@nestjs/cqrs';
import { SalesInvoiceDto } from '../../../sales-management/controllers/sales-invoice/sales-invoice-dto';

export class CreateNewSalesInvoiceCommand implements ICommand {
  constructor(
    public readonly salesInvoiceData: SalesInvoiceDto,
    public readonly clientHttpRequest: any,
  ) {}
}
