import {
  Injectable,
  HttpService,
  NotImplementedException,
  BadRequestException,
} from '@nestjs/common';
import { SalesInvoiceService } from '../../entities/sales-invoice/sales-invoice.service';
import { from, of, throwError } from 'rxjs';
import { switchMap, toArray, mergeMap, catchError } from 'rxjs/operators';
import { SettingsService } from '../../../system-settings/settings/settings.service';
import {
  PLEASE_SETUP_ACCOUNTING_SERVER,
  INVALID_ACCOUNT_ENTRY,
} from '../../../constants/messages';

@Injectable()
export class SubmitSalesInvoicePolicy {
  constructor(
    private readonly salesInvoiceService: SalesInvoiceService,
    private readonly http: HttpService,
    private readonly settingService: SettingsService,
  ) {}

  validateSalesInvoice(uuid, clientHttpRequest) {
    return from(this.salesInvoiceService.findOne({ uuid })).pipe(
      mergeMap((salesInvoice: any) => {
        return from(salesInvoice.invoiceItems).pipe(
          mergeMap((item: any) => {
            return this.validateInvoiceItems(item, clientHttpRequest);
          }),
          mergeMap(data => {
            return of(data);
          }),
          toArray(),
        );
      }),
    );
  }

  getHeaders(clientHttpRequest) {
    const accessToken = clientHttpRequest.token.accessToken;
    return {
      Authorization: 'Bearer ' + accessToken,
    };
  }

  validateInvoiceItems(item, clientHttpRequest) {
    return from(this.settingService.find()).pipe(
      switchMap((settings: any) => {
        if (!settings.accountingServer) {
          return throwError(
            new NotImplementedException(PLEASE_SETUP_ACCOUNTING_SERVER),
          );
        }
        const url =
          settings.accountingServer +
          VALIDATE_ACCOUNTS_ROUTE +
          item.account.accountNumber;
        return this.http
          .get(url, { headers: this.getHeaders(clientHttpRequest) })
          .pipe(
            switchMap((response: any) => {
              if (response.messages) {
                return throwError(
                  new BadRequestException({
                    error: INVALID_ACCOUNT_ENTRY,
                    at: item,
                  }),
                );
              } else {
                return of(item);
              }
            }),
            catchError(err => {
              return throwError(new BadRequestException(INVALID_ACCOUNT_ENTRY));
            }),
          );
      }),
    );
  }
}

export const VALIDATE_ACCOUNTS_ROUTE = '/account/v1/validate/';
