import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { SalesInvoiceService } from '../../entities/sales-invoice/sales-invoice.service';
import { SalesInvoiceSubmittedEvent } from './sales-invoice-submitted.event';

@EventsHandler(SalesInvoiceSubmittedEvent)
export class SalesInvoiceSubmittedHandler
  implements IEventHandler<SalesInvoiceSubmittedEvent> {
  constructor(private readonly salesInvoiceService: SalesInvoiceService) {}
  async handle(event: SalesInvoiceSubmittedEvent) {
    this.salesInvoiceService;
    // console.log({valid_entry_list : event.salesInvoiceData});
    // todo http request to insert all accounts.
  }
}
