import { IEvent } from '@nestjs/cqrs';

export class SalesInvoiceSubmittedEvent implements IEvent {
  constructor(public salesInvoiceData: any, public clientHttpRequest: any) {}
}
