import { IQuery } from '@nestjs/cqrs';

export class GetSalesInvoicesQuery implements IQuery {
  constructor(public uuid: string) {}
}
