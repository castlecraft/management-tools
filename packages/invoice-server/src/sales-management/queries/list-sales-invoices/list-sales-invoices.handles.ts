import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListSalesInvoicesQuery } from './list-sales-invoices.query';
import { CRUDOperationService } from '../../aggregates/query-services/crudoperation/crudoperation.service';

@QueryHandler(ListSalesInvoicesQuery)
export class ListSalesInvoicesHandler
  implements IQueryHandler<ListSalesInvoicesQuery> {
  constructor(private readonly crudService: CRUDOperationService) {}
  async execute(query: ListSalesInvoicesQuery) {
    return await this.crudService.listPaginate(
      query.model,
      query.offset,
      query.limit,
      query.search,
      query.query,
      query.searchField,
      query.sortQuery,
    );
  }
}
