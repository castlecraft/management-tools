import {
  Controller,
  Post,
  Body,
  Req,
  Get,
  Query,
  UseGuards,
  Param,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { SalesInvoiceDto } from './sales-invoice-dto';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { SalesInvoiceService } from '../../../sales-management/entities/sales-invoice/sales-invoice.service';
import { CreateNewSalesInvoiceCommand } from '../../../sales-management/commands/create-new-sales-invoice/create-new-sales-invoice.command';
import { ListSalesInvoicesQuery } from '../../queries/list-sales-invoices/list-sales-invoices.query';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { GetSalesInvoicesQuery } from '../../queries/get-sales-invoice/get-purchase-invoices.query';
import { UpdateSalesInvoiceCommand } from '../../commands/update-sales-invoice/update-sales-invoice.command';
import { SubmitSalesInvoiceCommand } from '../../commands/submit-sales-invoice/submit-sales-invoice.command';

@Controller('sales_invoice')
export class SalesInvoiceController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly salesInvoiceService: SalesInvoiceService,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(ValidationPipe)
  async createNewSalesInvoice(@Body() payload: SalesInvoiceDto, @Req() req) {
    return await this.commandBus.execute(
      new CreateNewSalesInvoiceCommand(payload, req),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(ValidationPipe)
  async updateSalesInvoice(@Body() payload: SalesInvoiceDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdateSalesInvoiceCommand(payload, req),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getSalesInvoice(@Param('uuid') uuid: string) {
    return await this.queryBus.execute(new GetSalesInvoicesQuery(uuid));
  }

  @Post('v1/submit/:uuid')
  @UseGuards(TokenGuard)
  async submitSalesInvoice(@Param('uuid') uuid: string, @Req() req) {
    return await this.commandBus.execute(
      new SubmitSalesInvoiceCommand(uuid, req),
    );
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  async getSalesInvoiceList(
    @Req() req,
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    const query: { createdBy?: string } = {};

    // query.createdBy = req.user.user;

    const sortQuery = { name: sort };
    return await this.queryBus.execute(
      new ListSalesInvoicesQuery(
        this.salesInvoiceService.getModel(),
        offset,
        limit,
        search,
        query,
        ['name', 'clientId'],
        sortQuery,
      ),
    );
  }
}
