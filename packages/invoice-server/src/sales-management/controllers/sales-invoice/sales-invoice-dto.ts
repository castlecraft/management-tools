import {
  IsString,
  IsNotEmpty,
  IsOptional,
  IsDateString,
  ValidateNested,
  IsNumber,
} from 'class-validator';
import { Type } from 'class-transformer';

export class SalesInvoiceDto {
  @IsString()
  @IsOptional()
  uuid: string;

  @IsString()
  @IsNotEmpty()
  invoiceName: string;

  @IsString()
  @IsNotEmpty()
  customerId: string;

  @IsString()
  @IsNotEmpty()
  customerName: string;

  @IsString()
  @IsDateString()
  date: Date;

  @IsString()
  @IsNotEmpty()
  invoiceNumber: string;

  @ValidateNested()
  @Type(() => InvoiceItems)
  invoiceItems: InvoiceItems[];

  @IsNumber()
  @IsNotEmpty()
  tax: number;

  @IsString()
  @IsNotEmpty()
  termsAndConditions: string;
}

export class InvoiceItems {
  @IsString()
  @IsOptional()
  account: AccountInterface;

  @IsString()
  @IsOptional()
  unit: string;

  @IsNumber()
  @IsOptional()
  quantity: number;

  @IsNumber()
  @IsOptional()
  cost: string;

  @IsNumber()
  @IsOptional()
  total: string;
}

export interface AccountInterface {
  accountNumber: string;
  accountName: string;
  accountType: string;
  parent: string;
  isGroup: boolean;
  uuid: string;
}
