import { IEvent } from '@nestjs/cqrs';
import { PurchaseInvoiceDto } from '../../controllers/purchase-invoice/purchase-invoice-dto';

export class PurchaseInvoiceUpdatedEvent implements IEvent {
  constructor(
    public purchaseInvoiceData: PurchaseInvoiceDto,
    public clientHttpRequest: any,
  ) {}
}
