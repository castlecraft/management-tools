import { IEvent } from '@nestjs/cqrs';
import { PurchaseInvoiceDto } from '../../../purchase-management/controllers/purchase-invoice/purchase-invoice-dto';

export class NewPurchaseInvoiceCreatedEvent implements IEvent {
  constructor(
    public purchaseInvoiceData: PurchaseInvoiceDto,
    public clientHttpRequest: any,
  ) {}
}
