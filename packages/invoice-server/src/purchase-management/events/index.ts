import { NewPurchaseInvoiceCreatedHandler } from './new-purchase-invoice-created-event/new-purchase-invoice-created.handler';
import { PurchaseInvoiceUpdatedHandler } from './purchase-invoice-updated-event/purchase-invoice-updated.handler';

export {
  NewPurchaseInvoiceCreatedEvent,
} from './new-purchase-invoice-created-event/new-purchase-invoice-created.event';

export const PurchaseManagementEvents = [
  NewPurchaseInvoiceCreatedHandler,
  PurchaseInvoiceUpdatedHandler,
];
