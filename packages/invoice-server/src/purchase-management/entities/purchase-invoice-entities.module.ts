import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PurchaseInvoiceService } from './purchase-invoice/purchase-invoice.service';
import {
  PURCHASE_INVOICE,
  PurchaseInvoiceSchema,
} from './purchase-invoice/purchase-invoice.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: PURCHASE_INVOICE, schema: PurchaseInvoiceSchema },
    ]),
  ],
  providers: [PurchaseInvoiceService],
  exports: [PurchaseInvoiceService],
})
export class PurchaseInvoiceEntitiesModule {}
