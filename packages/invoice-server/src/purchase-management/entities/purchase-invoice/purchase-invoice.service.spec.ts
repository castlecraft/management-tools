import { Test, TestingModule } from '@nestjs/testing';
import { PurchaseInvoiceService } from './purchase-invoice.service';
import { getModelToken } from '@nestjs/mongoose';
import { PURCHASE_INVOICE } from './purchase-invoice.schema';

describe('PurchaseInvoiceService', () => {
  let service: PurchaseInvoiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PurchaseInvoiceService,
        {
          provide: getModelToken(PURCHASE_INVOICE),
          useValue: {}, // use mock values
        },
      ],
    }).compile();

    service = module.get<PurchaseInvoiceService>(PurchaseInvoiceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
