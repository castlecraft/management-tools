import { IQuery } from '@nestjs/cqrs';
import { Model } from 'mongoose';
import { PurchaseInvoice } from '../../../purchase-management/entities/purchase-invoice/purchase-invoice.interface';

export class ListPurchaseInvoicesQuery implements IQuery {
  constructor(
    public model: Model<PurchaseInvoice>,
    public offset: number,
    public limit: number,
    public search: string,
    public query: { createdBy?: string },
    public searchField: string[],
    public sortQuery: { name: string },
  ) {}
}
