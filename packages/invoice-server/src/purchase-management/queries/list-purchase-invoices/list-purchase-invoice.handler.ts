import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { ListPurchaseInvoicesQuery } from './list-purchase-invoices.query';
import { CRUDOperationService } from '../../aggregates/query-services/crudoperation/crudoperation.service';

@QueryHandler(ListPurchaseInvoicesQuery)
export class ListPurchaseInvoicesHandler
  implements IQueryHandler<ListPurchaseInvoicesQuery> {
  constructor(private readonly crudService: CRUDOperationService) {}
  async execute(query: ListPurchaseInvoicesQuery) {
    return await this.crudService.listPaginate(
      query.model,
      query.offset,
      query.limit,
      query.search,
      query.query,
      query.searchField,
      query.sortQuery,
    );
  }
}
