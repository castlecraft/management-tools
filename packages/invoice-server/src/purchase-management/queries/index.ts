import { ListPurchaseInvoicesHandler } from './list-purchase-invoices/list-purchase-invoice.handler';
import { GetPurchaseInvoicesHandler } from './get-purchase-invoice/get-purchase-invoice.handler';

export const PurchaseManagementQueries = [
  ListPurchaseInvoicesHandler,
  GetPurchaseInvoicesHandler,
];
