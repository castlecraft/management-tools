import { ICommand } from '@nestjs/cqrs';
import { PurchaseInvoiceDto } from '../../../purchase-management/controllers/purchase-invoice/purchase-invoice-dto';

export class CreateNewPurchaseInvoiceCommand implements ICommand {
  constructor(
    public readonly purchaseInvoiceData: PurchaseInvoiceDto,
    public readonly clientHttpRequest: any,
  ) {}
}
