import { ICommand } from '@nestjs/cqrs';
import { PurchaseInvoiceDto } from '../../controllers/purchase-invoice/purchase-invoice-dto';

export class UpdatePurchaseInvoiceCommand implements ICommand {
  constructor(
    public readonly purchaseInvoiceData: PurchaseInvoiceDto,
    public readonly clientHttpRequest: any,
  ) {}
}
