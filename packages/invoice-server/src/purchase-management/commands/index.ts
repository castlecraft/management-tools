import { CreateNewPurchaseInvoiceHandler } from './create-new-purchase-invoice/create-new-invoice.handler';
import { UpdatePurchaseInvoiceHandler } from './update-purchase-invoice/update-purchase-invoice.handler';

export const PurchaseManagementCommands = [
  CreateNewPurchaseInvoiceHandler,
  UpdatePurchaseInvoiceHandler,
];
