import { PurchaseInvoiceManagerAggregate } from './purchase-invoice-manager-aggregate/purchase-invoice-manager-aggregate';
import { CRUDOperationService } from './query-services/crudoperation/crudoperation.service';

export {
  PurchaseInvoiceManagerAggregate,
} from './purchase-invoice-manager-aggregate/purchase-invoice-manager-aggregate';
export const PurchaseManagementAggregates = [
  PurchaseInvoiceManagerAggregate,
  CRUDOperationService,
];
