import { AggregateRoot } from '@nestjs/cqrs';
import { NewPurchaseInvoiceCreatedEvent } from '../../../purchase-management/events';
import * as uuidv4 from 'uuid/v4';
import { PurchaseInvoiceUpdatedEvent } from '../../events/purchase-invoice-updated-event/purchase-invoice-updated.event';

export class PurchaseInvoiceManagerAggregate extends AggregateRoot {
  constructor() {
    super();
  }

  async createNewInvoice(purchaseInvoiceData, clientHttpRequest) {
    const uuid = uuidv4();
    purchaseInvoiceData.uuid = uuid;
    return this.apply(
      new NewPurchaseInvoiceCreatedEvent(
        purchaseInvoiceData,
        clientHttpRequest,
      ),
    );
  }

  async updatePurchaseInvocie(purchaseInvoiceData, clientHttpRequest) {
    return this.apply(
      new PurchaseInvoiceUpdatedEvent(purchaseInvoiceData, clientHttpRequest),
    );
  }
}
