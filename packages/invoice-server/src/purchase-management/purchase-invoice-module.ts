import { Global, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { PurchaseManagementAggregates } from './aggregates';
import { PurchaseManagementEvents } from './events';
import { PurchaseManagementCommands } from './commands';
import { PurchaseInvoiceController } from './controllers/purchase-invoice/purchase-invoice.controller';
import { PurchaseInvoiceEntitiesModule } from './entities/purchase-invoice-entities.module';
import { PurchaseManagementQueries } from './queries';

@Global()
@Module({
  imports: [CqrsModule, PurchaseInvoiceEntitiesModule],
  controllers: [PurchaseInvoiceController],
  providers: [
    ...PurchaseManagementAggregates,
    ...PurchaseManagementEvents,
    ...PurchaseManagementCommands,
    ...PurchaseManagementQueries,
  ],
  exports: [PurchaseInvoiceEntitiesModule],
})
export class PurchaseInvoiceModule {}
