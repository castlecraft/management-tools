import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TYPEORM_CONNECTION } from './models/typeorm.connection';
import { ConfigModule } from './config/config.module';
import { ModelsModule } from './models/models.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SetupService } from './system-settings/setup/setup.service';
import { SetupController } from './system-settings/setup/setup.controller';
import { EnquiryModule } from './enquiry/enquiry.module';

@Module({
  imports: [
    ConfigModule,
    ModelsModule,
    TypeOrmModule.forRoot(TYPEORM_CONNECTION),
    EnquiryModule,
    HttpModule,
  ],
  controllers: [AppController, SetupController],
  providers: [AppService, SetupService],
})
export class AppModule {}
