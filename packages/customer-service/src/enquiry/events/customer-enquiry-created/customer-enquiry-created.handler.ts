import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { CustomerEnquiryCreatedEvent } from './customer-enquiry-created.event';

@EventsHandler(CustomerEnquiryCreatedEvent)
export class CustomerEnquiryCreatedHandler
  implements IEventHandler<CustomerEnquiryCreatedEvent> {
  handle(event: CustomerEnquiryCreatedEvent) {
    const { provider } = event;
    provider
      .save()
      .then(done => {})
      .catch(error => {});
  }
}
