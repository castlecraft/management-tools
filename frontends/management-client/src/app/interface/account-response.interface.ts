export interface CreateAccountResponse {
  accountNumber: string;
  accountName: string;
  accountType: string;
  parent: string;
  isGroup: boolean;
  uuid: string;
}
