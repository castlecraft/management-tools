import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { SELECTED_DOMAIN } from '../../constants/storage';
import { OAuthService } from 'angular-oauth2-oidc';
import { StorageService } from '../../common/services/storage-service/storage.service';

@Injectable({
  providedIn: 'root',
})
export class ReportsService {
  authorizationHeaders: HttpHeaders;
  constructor(
    private readonly storageService: StorageService,
    private http: HttpClient,
    private oauthService: OAuthService,
  ) {
    this.authorizationHeaders = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }
  appUrl = this.storageService.getInfo(SELECTED_DOMAIN);

  getProfitReport(fromTime, toTime) {
    const url = this.appUrl + '/account-reports/v1/getProfitReports';
    const params = new HttpParams()
      .set('fromTime', fromTime)
      .set('toTime', toTime);

    return this.http.get(url, {
      params,
      headers: this.authorizationHeaders,
      responseType: 'text',
    });
  }

  getBalanceReports(fromTime, toTime) {
    const url = this.appUrl + '/account-reports/v1/getBalanceReports';
    const params = new HttpParams()
      .set('fromTime', fromTime)
      .set('toTime', toTime);

    return this.http.get(url, {
      params,
      headers: this.authorizationHeaders,
      responseType: 'text',
    });
  }

  getExpenditureReport(fromTime, toTime) {
    const url = this.appUrl + '/account-reports/v1/getExpenditureReport';
    const params = new HttpParams()
      .set('fromTime', fromTime)
      .set('toTime', toTime);

    return this.http.get(url, {
      params,
      headers: this.authorizationHeaders,
      responseType: 'text',
    });
  }

  getTimeReport(fromTime, toTime) {
    const url = this.appUrl + '/account-reports/v1/timeBasedReport';
    const params = new HttpParams()
      .set('fromTime', fromTime)
      .set('toTime', toTime);

    return this.http.get(url, {
      params,
      headers: this.authorizationHeaders,
      responseType: 'text',
    });
  }
}
