import { JournalEntryComponent } from './journal-entry.component';
import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OAuthService } from 'angular-oauth2-oidc';
import { oauthServiceStub } from '../../common/testing-helpers';
import { JournalEntryService } from './journal-entry.service';
import { AuthGuard } from '../../common/guards/auth.guard';
import { HttpErrorHandler } from '../../http-error-handler.service';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { MaterialModule } from '../../shared-imports/material/material.module';
import { ListingService } from 'src/app/common/services/listing-service/listing.service';
import { of } from 'rxjs';

const listingService: Partial<ListingService> = {
  findModels: (...args) => of([]),
};

describe('JournalEntryComponent', () => {
  let component: JournalEntryComponent;
  let fixture: ComponentFixture<JournalEntryComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [JournalEntryComponent],
      imports: [
        NoopAnimationsModule,
        MaterialModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: JournalEntryService,
          useValue: {
            getJe: (...args) => '',
            getNewJE: (...args) => '',
          },
        },
        {
          provide: AuthGuard,
          useValue: {},
        },
        {
          provide: HttpErrorHandler,
          useValue: {
            createHandleError: (...args) => {},
          },
        },
        {
          provide: StorageService,
          useValue: {
            getServiceURL: (...args) => ({}),
            getDomainList: (...args) => {},
            getInfo: (...args) => {},
          },
        },
        {
          provide: ListingService,
          useValue: listingService,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  afterAll(() => {
    TestBed.resetTestingModule();
  });
});
