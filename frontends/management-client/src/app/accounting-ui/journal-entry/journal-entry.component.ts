import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import {
  ENTRY_TYPE,
  CREATED,
  CLOSE,
  JOURNAL_ENTRY,
  DISPLAYED_COLUMNS,
  ELEMENT_DATA,
  ACCOUNTING_SERVER,
} from '../../constants/storage';
import { JournalEntryService } from './journal-entry.service';
import { CreateJeResponse } from './journal-entry.interface';
import {
  MatSnackBar,
  MatTableDataSource,
  MatPaginator,
  MatSort,
} from '@angular/material';
import { ListingService } from '../../common/services/listing-service/listing.service';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { ACCOUNT_CREATED } from '../../constants/messages';

@Component({
  selector: 'app-journalentry',
  templateUrl: './journal-entry.component.html',
  styleUrls: ['./journal-entry.component.css'],
})
export class JournalEntryComponent implements OnInit {
  uuid: string;
  model: string;
  entryType: string;
  debitSum: number = 0;
  creditSum: number = 0;
  amount: number;
  account: string;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  journalEntryTransactionId: number;
  types = ENTRY_TYPE;
  filter: string;
  sortOrder: 'ASC' | 'DSC';
  pageNumber: number;
  pageSize: number;
  query: string;
  displayedColumns = DISPLAYED_COLUMNS;
  elementData = ELEMENT_DATA;
  dataSource = new MatTableDataSource(this.elementData);
  bulkCSV: File;
  treeControl;
  accountForm = new FormGroup({
    bulkCSV: new FormControl(this.bulkCSV),
  });
  disableJournalEntryForm: boolean = false;
  jeForm: FormGroup;
  editing = {};
  responseData: any[] = [];
  data;
  domainList: string[];
  domain: string;
  dropDownData: any[] | any;
  createJeDto: CreateJeResponse;

  constructor(
    private journalEntryService: JournalEntryService,
    private router: Router,
    private snackBar: MatSnackBar,
    private accountService: ListingService,
    private activatedRoute: ActivatedRoute,
    private readonly storageService: StorageService,
    private formBuilder: FormBuilder, // private snackbar: MatSnackBar,
  ) {
    // TODO: get data from server

    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.model = route.url.split('/')[1];
      });
  }

  ngOnInit() {
    this.domainList = this.storageService.getDomainList(ACCOUNTING_SERVER);
    if (this.activatedRoute.snapshot.params.id !== 'new') {
      this.journalEntryTransactionId = this.activatedRoute.snapshot.params.id;
    }

    this.accountService
      .findModels(
        (this.model = 'account'),
        this.filter,
        this.sortOrder,
        this.pageNumber,
        this.pageSize,
        (this.query = `"isGroup" = 'false'`),
      )
      .subscribe({
        next: (data: any) => {
          this.dropDownData = data.docs;
        },
        error: err => {},
      });

    if (this.journalEntryTransactionId) {
      this.dataSource.data = this.responseData;
    }

    this.jeForm = this.formBuilder.group({
      entryType: '',
      amount: 0,
      account: 1,
    });

    if (this.journalEntryTransactionId) {
      this.journalEntryService
        .getJe(this.model, this.journalEntryTransactionId)
        .subscribe({
          next: response => {
            this.dataSource.data = response;
            this.dataSource._updateChangeSubscription();
          },
        });
    }
  }

  onFileChanged(event) {
    this.disableJournalEntryForm = true;
    this.bulkCSV = event.target.files[0];
  }

  manualEntry() {
    this.disableJournalEntryForm = false;
    this.bulkCSV = null;
  }

  uploadCSV() {
    this.journalEntryService.uploadCSV(this.bulkCSV).subscribe({
      next: res => {
        if (res) this.snackBar.open(ACCOUNT_CREATED, CLOSE, { duration: 2500 });
      },
      error: err => {
        this.snackBar.open(err.error.message, CLOSE, { duration: 2500 });
      },
    });
  }

  removeRow(index) {
    this.dataSource.data.splice(index, 1);
    this.dataSource._updateChangeSubscription();
  }

  addRow() {
    this.elementData.push({
      account: '',
      entryType: '',
      amount: 0,
    });
    this.dataSource._updateChangeSubscription();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  createJe() {
    this.journalEntryService.createJe(this.dataSource.data).subscribe({
      next: res => {
        this.snackBar.open(JOURNAL_ENTRY + CREATED, CLOSE, {
          duration: 2500,
        });
        this.router.navigateByUrl('/journalentry');
      },
      error: err => {
        this.snackBar.open('err', CLOSE, { duration: 2500 });
      },
    });
  }

  async getDomainData() {
    const url = this.accountForm.controls.domain.value;
    if (!url) {
      this.snackBar.open('please Select a Domain', CLOSE, { duration: 3500 });
      return;
    }
    await this.storageService.setSelectedDomain(url.url);
    this.snackBar.open('Domain Set To ' + url.type, CLOSE, { duration: 2500 });
  }
}
