import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { SELECTED_DOMAIN } from '../../constants/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from '../../common/services/storage-service/storage.service';

@Injectable({
  providedIn: 'root',
})
export class JournalEntryService {
  authorizationHeader: HttpHeaders;
  model = 'journalentry';
  constructor(
    private readonly storageService: StorageService,
    private readonly http: HttpClient,
    private readonly oauthService: OAuthService,
  ) {
    this.authorizationHeader = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  getJe(model, journalEntryTransactionId) {
    const accountServer = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${accountServer}/${this.model}/v1/getOne/${journalEntryTransactionId}`;
    // TODO: Interface for je response;
    return this.http.get<any>(url, { headers: this.authorizationHeader });
  }

  uploadCSV(file) {
    const uploadData = new FormData();
    const accountServer = this.storageService.getInfo(SELECTED_DOMAIN);
    uploadData.append('file', file, file.name);
    const url = accountServer + '/journalentry/v1/bulkCsv';
    return this.http.post(url, uploadData, {
      headers: this.authorizationHeader,
    });
  }

  createJe(rows) {
    const accountServer = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${accountServer}/${this.model}/v1/create`;
    return this.http.post(
      url,
      { accounts: rows },
      { headers: this.authorizationHeader },
    );
  }
}
