import { TestBed } from '@angular/core/testing';
import { JournalEntryService } from './journal-entry.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OAuthService } from 'angular-oauth2-oidc';
import { HttpErrorHandler } from '../../http-error-handler.service';
import { oauthServiceStub } from '../../common/testing-helpers';

describe('JournalEntryService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HttpErrorHandler,
          useValue: {
            handleError<T>(...args) {},
            createHandleError(...args) {},
          },
        },
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: JournalEntryService,
          useValue: JournalEntryService,
        },
      ],
    }),
  );
  it('should be created', () => {
    const service: JournalEntryService = TestBed.get(JournalEntryService);
    expect(service).toBeTruthy();
  });
});
