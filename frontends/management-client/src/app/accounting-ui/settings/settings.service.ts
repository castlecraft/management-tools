import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { SELECTED_DOMAIN } from '../../constants/storage';
import { StorageService } from '../../common/services/storage-service/storage.service';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  authorizationHeader: HttpHeaders;
  model = 'settings';
  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    private readonly oauthService: OAuthService,
  ) {
    this.setupHeaders();
  }

  setupHeaders() {
    this.authorizationHeader = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  setupAccount() {
    this.setupHeaders();
    const domainUrl = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${domainUrl}/account-setup/v1/oneTimeSetup`;
    return this.http.post(url, undefined, {
      headers: this.authorizationHeader,
    });
  }

  getSettings() {
    const domainUrl = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${domainUrl}/settings/v1/getSettings`;
    const accountSettings = this.http.get(url, {
      headers: this.authorizationHeader,
    });
    return accountSettings;
  }

  setPeriodClosing(param) {
    const domainUrl = this.storageService.getInfo(SELECTED_DOMAIN);
    const url = `${domainUrl}/settings/v1/periodClosing`;
    return this.http.post(
      url,
      { body: { periodClosing: param } },
      { headers: this.authorizationHeader },
    );
  }
}
