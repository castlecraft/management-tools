import { Component, OnInit } from '@angular/core';
import { SettingsService } from './settings.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import {
  CLOSING_OPTIONS,
  PERIOD_CLOSING,
  UPDATED,
  CLOSE,
  ACCOUNTING_SERVER,
  ACCOUNT_SETUP,
} from './../../constants/storage';
import { MatSnackBar } from '@angular/material';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { AppService } from '../../app.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
  periodClosing: string;
  closingOptions: string[] = [];
  settingsForm: FormGroup;
  domainList: string[] = [];
  accountSetup: boolean;
  selectedDomain: any;
  constructor(
    private readonly storageService: StorageService,
    private readonly settingsService: SettingsService,
    private readonly formBuilder: FormBuilder,
    private readonly snackBar: MatSnackBar,
    private readonly appService: AppService,
  ) {}

  ngOnInit() {
    this.selectedDomain = this.storageService.getSelectedDomain();
    this.domainList = this.storageService.getDomainList(ACCOUNTING_SERVER);
    this.settingsForm = this.formBuilder.group({
      accountSetup: '',
      periodClosing: '',
      domain: '',
    });
    this.closingOptions = CLOSING_OPTIONS;
    this.settingsService.getSettings().subscribe({
      next: (response: any[]) => {
        this.populateSettingsForm(response);
      },
    });
    this.storageService.getSelectedDomainSettings().subscribe({
      next: (data: any) => {
        this.accountSetup = data.accountSetup;
      },
      error: err => {
        this.snackBar.open(err.message, CLOSE, { duration: 2500 });
      },
    });
  }

  setPeriodClosing() {
    const newClosing = this.settingsForm.controls.periodClosing.value;
    this.settingsService.setPeriodClosing(newClosing).subscribe({
      next: (response: any[]) => {
        this.snackBar.open(PERIOD_CLOSING + UPDATED, CLOSE, {
          duration: 2500,
        });
      },
    });
  }

  async getDomainData() {
    const url = this.settingsForm.controls.domain.value;
    if (!url) {
      this.snackBar.open('please Select a Domain', CLOSE, { duration: 3500 });
      return;
    }
    this.selectedDomain = url;
    await this.storageService.setSelectedDomain(url);
    await this.appService.getAccountingInfo().subscribe({
      next: success => {},
      error: error => {},
    });
    this.storageService.getSelectedDomainSettings().subscribe({
      next: (settings: any) => {
        this.accountSetup = settings.accountSetup;
        this.periodClosing = settings.periodClosing;
        this.snackBar.open('Domain Set To ' + settings.type, CLOSE, {
          duration: 3500,
        });
      },
      error: err => {
        this.snackBar.open(err.message, CLOSE, { duration: 2500 });
      },
    });
  }

  setupAccount() {
    this.settingsService.setupAccount().subscribe({
      next: (response: any) => {
        this.accountSetup = response;
        this.snackBar.open(ACCOUNT_SETUP, CLOSE, {
          duration: 3500,
        });
      },
      error: err => {},
    });
  }

  populateSettingsForm(response) {
    this.settingsForm.controls.periodClosing.setValue(
      response[0].periodClosing,
    );
  }
}
