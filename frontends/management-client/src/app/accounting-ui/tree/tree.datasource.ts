import { TreeService } from './tree.service';
import { MatTreeNestedDataSource } from '@angular/material';
import { AccountNode } from './account-node';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { CollectionViewer } from '@angular/cdk/collections';
import { catchError, finalize } from 'rxjs/operators';

export class TreeDataSource implements MatTreeNestedDataSource<AccountNode> {
  _data = new BehaviorSubject<AccountNode[]>([]);
  data: AccountNode[] = [];
  private loadingData = new BehaviorSubject<boolean>(false);

  constructor(private readonly treeService: TreeService) {}

  connect(collectionViewer: CollectionViewer): Observable<AccountNode[]> {
    return this._data.asObservable();
  }

  disconnect(): void {
    this._data.complete();
    this.loadingData.complete();
  }

  loadNodes(node?: AccountNode) {
    this.loadingData.next(true);
    if (node && node.accountNumber) {
      this.treeService
        .getChildrens(node.accountNumber)
        .pipe(
          catchError(() => of([])),
          finalize(() => this.loadingData.next(false)),
        )
        .subscribe((accounts: AccountNode[]) => {
          this.data.concat(accounts);
          this._data.next(accounts);
        });
    } else {
      this.treeService
        .getParents()
        .pipe(
          catchError(() => of([])),
          finalize(() => this.loadingData.next(false)),
        )
        .subscribe((accounts: AccountNode[]) => this._data.next(accounts));
    }
  }
}
