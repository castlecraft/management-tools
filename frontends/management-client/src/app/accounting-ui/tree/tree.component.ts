import { Component } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatSnackBar } from '@angular/material';
import { TreeService } from './tree.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { TreeDataSource } from './tree.datasource';
import { AccountNode } from './account-node';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { ACCOUNTING_SERVER, CLOSE } from '../../constants/storage';
import { ACCOUNT_CREATED } from '../../constants/messages';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css'],
})
export class TreeComponent {
  bulkCSV: File;
  domainList: string[];
  dataSource: TreeDataSource;
  domain: string;
  selectedDomain: any;
  treeControl;
  accountForm = new FormGroup({
    bulkCSV: new FormControl(this.bulkCSV),
    domain: new FormControl(this.domain),
  });

  hasChild = (_: number, node: AccountNode) => true;

  constructor(
    private readonly treeService: TreeService,
    private readonly snackBar: MatSnackBar,
    private readonly router: Router,
    private readonly storageService: StorageService,
  ) {
    this.treeControl = new FlatTreeControl<AccountNode>(
      node => node.level,
      node => node.expandable,
    );
  }

  ngOnInit() {
    this.selectedDomain = this.storageService.getSelectedDomain();
    this.domainList = this.storageService.getDomainList(ACCOUNTING_SERVER);
    this.dataSource = new TreeDataSource(this.treeService);
    this.dataSource.loadNodes();
  }

  onFileChanged(event) {
    this.bulkCSV = event.target.files[0];
  }

  loadChildren(node) {
    this.dataSource.loadNodes(node);
  }

  uploadCSV() {
    this.treeService.uploadCSV(this.bulkCSV).subscribe({
      next: res => {
        if (res) this.snackBar.open(ACCOUNT_CREATED, CLOSE, { duration: 2500 });
        this.router.navigateByUrl('/account');
      },
      error: err => {
        this.snackBar.open(err.error.message, CLOSE, { duration: 2500 });
      },
    });
  }

  async getDomainData() {
    const url = this.accountForm.controls.domain.value;
    if (!url) {
      this.snackBar.open('please Select a Domain', CLOSE, { duration: 3500 });
      return;
    }
    this.selectedDomain = url;
    await this.storageService.setSelectedDomain(url);
    this.snackBar.open('Domain Set To ' + url.type, CLOSE, { duration: 2500 });
  }
}
