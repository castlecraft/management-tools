import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { AccountComponent } from './account.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OAuthService } from 'angular-oauth2-oidc';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { MaterialModule } from '../../shared-imports/material/material.module';
import { HttpErrorHandler } from '../../http-error-handler.service';
import { FormService } from '../../common/services/form-service/form.service';
import { oauthServiceStub } from '../../common/testing-helpers';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { ListingService } from '../../common/services/listing-service/listing.service';

describe('AccountComponent', () => {
  let component: AccountComponent;
  let fixture: ComponentFixture<AccountComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        BrowserDynamicTestingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
      ],
      declarations: [AccountComponent],
      providers: [
        {
          provide: HttpErrorHandler,
          useValue: {
            createHandleError: (...args) => 'mock',
          },
        },
        {
          provide: FormService,
          useValue: {},
        },
        {
          provide: ListingService,
          useValue: {
            findModels: (...args) => of([]),
          },
        },
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: of({ uuid: 'new' }),
            },
          },
        },
        {
          provide: StorageService,
          useValue: {
            getServiceURL: (...args) => ({}),
            getInfo: (...args) => {},
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  afterAll(() => {
    TestBed.resetTestingModule();
  });
});
