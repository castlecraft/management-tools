import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter, map, debounceTime } from 'rxjs/operators';
import { ACCOUNT_TYPES, NEW_ID } from '../../constants/storage';
import { FormService } from '../../common/services/form-service/form.service';
import { ListingService } from '../../common/services/listing-service/listing.service';
import { CreateAccountResponse } from '../../interface/account-response.interface';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
})
export class AccountComponent implements OnInit {
  uuid: string;
  model: string;
  search: string = '';
  accountNumber: string;
  accountName: string;
  accountType: string;
  isGroup: boolean;
  parent: string;
  types = ACCOUNT_TYPES;
  accounts;
  disableJournalEntryForm: boolean = false;
  bulkCSV: File;
  treeControl;
  csvForm = new FormGroup({
    bulkCSV: new FormControl(this.bulkCSV),
  });
  accountForm: FormGroup;

  constructor(
    private formService: FormService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private listService: ListingService,
  ) {
    this.uuid = this.activatedRoute.snapshot.params.uuid;
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.model = route.url.split('/')[1];
      });
  }

  ngOnInit() {
    this.accountForm = this.formBuilder.group({
      accountNumber: '',
      accountName: '',
      accountType: '',
      isGroup: '',
      parent: '',
    });

    this.subscribeListAccounts();

    if (this.uuid && this.uuid !== NEW_ID) {
      this.subscribeGetAccount(this.uuid);
    } else {
      this.accountForm.controls.isGroup.enable();
    }
  }

  searchKeyUp() {
    this.search = this.accountForm.controls.parent.value;
    this.subscribeListAccounts();
  }

  subscribeListAccounts() {
    this.accounts = this.listService
      .findModels(this.model, this.search, '', 0, 10, `"isGroup" = true`)
      .pipe(
        map((resp: any) => {
          if (resp.docs) {
            return resp.docs.map(account => account.accountNumber);
          }
        }),
        debounceTime(1000),
      );
  }

  subscribeGetAccount(uuid: string) {
    this.formService.getAccount(this.model, this.uuid).subscribe({
      next: response => {
        if (response) {
          this.populateAccountForm(response);
        }
      },
    });
  }

  createAccount() {
    this.formService
      .createAccount(
        this.model,
        this.accountForm.controls.accountNumber.value,
        this.accountForm.controls.accountName.value,
        this.accountForm.controls.accountType.value,
        this.accountForm.controls.isGroup.value || false,
        this.accountForm.controls.parent.value,
      )
      .subscribe({
        next: (accountResponse: CreateAccountResponse) => {
          this.accountForm.controls.parent.setValue('');
          this.router.navigateByUrl('/account');
        },
      });
  }

  onFileChanged(event) {
    this.disableJournalEntryForm = true;
    this.bulkCSV = event.target.files[0];
  }

  manualEntry() {
    this.disableJournalEntryForm = false;
    this.bulkCSV = null;
  }

  uploadCSV() {
    this.formService.uploadCSV(this.bulkCSV).subscribe({
      next: res => {},
      error: err => {},
    });
  }

  updateAccount() {
    this.formService
      .updateAccount(
        this.uuid,
        this.model,
        this.accountForm.controls.accountName.value,
        this.accountForm.controls.accountNumber.value,
      )
      .subscribe({
        next: response => {
          this.router.navigateByUrl('/listing/account');
        },
      });
  }

  populateAccountForm(account) {
    this.accountNumber = account.accountNumber;
    this.accountName = account.accountName;
    this.accountType = account.accountType;
    this.isGroup = account.isGroup;
    this.parent = account.parent;
    this.accountForm.controls.accountNumber.setValue(account.accountNumber);
    this.accountForm.controls.accountName.setValue(account.accountName);
    this.accountForm.controls.accountType.setValue(account.accountType);
    this.accountForm.controls.parent.setValue(account.parent);
    this.accountForm.controls.isGroup.setValue(account.isGroup);
    this.accountForm.controls.accountType.disable();
    this.accountForm.controls.isGroup.disable();
    this.accountForm.controls.accountNumber.disable();
    this.accountForm.controls.parent.disable();
  }
}
