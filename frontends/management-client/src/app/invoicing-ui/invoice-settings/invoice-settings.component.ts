import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import {
  CLOSE,
  SELECTED_INVOICE_SERVER,
  INVOICING_SERVER,
  ACCOUNTING_SERVER,
} from '../../constants/storage';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { InvoiceSettingsService } from './invoice-settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './invoice-settings.component.html',
  styleUrls: ['./invoice-settings.component.css'],
})
export class InvoiceSettingsComponent implements OnInit {
  settingsForm = new FormGroup({
    accountingServer: new FormControl(),
    domain: new FormControl(),
  });
  domainList: string[] = [];
  accountingServerList: string[] = [];
  accountingServer: boolean;
  selectedServer: string;
  setupComplete: boolean = true;
  selectedDomain: any;
  constructor(
    private readonly snackBar: MatSnackBar,
    private readonly storageService: StorageService,
    private readonly settingsService: InvoiceSettingsService,
  ) {}

  ngOnInit() {
    this.populateSettingsForm();
    this.selectedServer = 'Accounting Server';
    this.selectedDomain = this.storageService.getSelectedDomain(
      SELECTED_INVOICE_SERVER,
    );
    this.domainList = this.storageService.getDomainList(INVOICING_SERVER);
    this.accountingServerList = this.storageService.getDomainList(
      ACCOUNTING_SERVER,
    );
  }

  async getDomainData() {
    const url = this.settingsForm.controls.domain.value;
    this.storageService.setSelectedDomain(url, SELECTED_INVOICE_SERVER);
    this.selectedServer = 'Accounting Server';
    this.settingsForm.controls.accountingServer.enable();
    this.setupComplete = true;
    this.populateSettingsForm();
  }

  populateSettingsForm() {
    this.settingsService.getSelectedDomainSettings().subscribe({
      next: (response: any) => {
        this.settingsForm.controls.accountingServer.enable();
        if (response.accountingServer) {
          this.setupComplete = false;
          const data: any = this.storageService.domainJson(
            response.accountingServer,
          );
          this.settingsForm.controls.accountingServer.setValue(data);
          this.settingsForm.controls.accountingServer.disable();
          this.selectedServer = data[0].name;
        } else {
          this.snackBar.open(
            'Please Set accounting server for selected invoice server',
            CLOSE,
            { duration: 2500 },
          );
        }
      },
      error: err => {},
    });
  }

  setAccountingServer() {
    this.settingsService
      .setAccountingServer(this.settingsForm.controls.accountingServer.value)
      .subscribe({
        next: (response: any) => {
          this.snackBar.open('settings updated', CLOSE, { duration: 2500 });
          this.populateSettingsForm();
        },
        error: err => {
          this.snackBar.open('update settings failed', CLOSE, {
            duration: 2500,
          });
        },
      });
  }
}
