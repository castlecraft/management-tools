import { LayoutModule } from '@angular/cdk/layout';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavigationComponent } from './navigation.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { OAuthService } from 'angular-oauth2-oidc';
import { oauthServiceStub } from '../../common/testing-helpers';
import { AppComponent } from '../../app.component';
import { SettingsService } from '../../accounting-ui/settings/settings.service';
import { ListingService } from '../../common/services/listing-service/listing.service';
import { StorageService } from '../../common/services/storage-service/storage.service';
import { AppService } from '../../app.service';
import { MaterialModule } from '../../shared-imports/material/material.module';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavigationComponent],
      imports: [
        BrowserAnimationsModule,
        MaterialModule,
        LayoutModule,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: AppComponent,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {
            setupAccount: (...args) => of({}),
          },
        },
        {
          provide: ListingService,
          useValue: {
            getMessage: (...args) => of({}),
          },
        },
        {
          provide: StorageService,
          useValue: {
            getInfo: (...args) => true,
          },
        },
        {
          provide: AppService,
          useValue: {},
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
