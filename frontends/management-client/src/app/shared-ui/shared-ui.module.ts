import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListingComponent } from './listing/listing.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { ListingService } from '../common/services/listing-service/listing.service';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [ListingComponent, NavigationComponent, HomeComponent],
  imports: [SharedImportsModule, CommonModule],
  providers: [ListingService],
  exports: [ListingComponent, NavigationComponent, HomeComponent],
})
export class SharedUIModule {}
