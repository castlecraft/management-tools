import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './shared-ui/home/home.component';
import { ListingComponent } from './shared-ui/listing/listing.component';
import { AccountComponent } from './accounting-ui/account/account.component';
import { AuthGuard } from './common/guards/auth.guard';
import { ReportsComponent } from './accounting-ui/reports/reports.component';
import { TreeComponent } from './accounting-ui/tree/tree.component';
import { JournalEntryComponent } from './accounting-ui/journal-entry/journal-entry.component';
import { SalesInvoiceComponent } from './invoicing-ui/sales-invoice/sales-invoice.component';
import { PurchaseInvoiceComponent } from './invoicing-ui/purchase-invoice/purchase-invoice.component';
import { SettingsComponent } from './accounting-ui/settings/settings.component';
import { InvoiceSettingsComponent } from './invoicing-ui/invoice-settings/invoice-settings.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },

  { path: 'account', component: ListingComponent },
  {
    path: 'listing/:listingModel',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'sales_invoice',
    component: SalesInvoiceComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'purchase_invoice',
    component: PurchaseInvoiceComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'sales_invoice/:uuid',
    component: SalesInvoiceComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'purchase_invoice/:uuid',
    component: PurchaseInvoiceComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'account/:uuid',
    component: AccountComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'reports',
    component: ReportsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'settings/account',
    component: SettingsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'settings/invoice',
    component: InvoiceSettingsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'tree/account',
    component: TreeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'journalentry/:id',
    component: JournalEntryComponent,
    canActivate: [AuthGuard],
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
