import { Injectable } from '@angular/core';
import {
  CLIENT_ID,
  REDIRECT_URI,
  SILENT_REFRESH_REDIRECT_URI,
  LOGIN_URL,
  ISSUER_URL,
  USER_UUID,
  APP_URL,
  SERVICES,
  ACCOUNTING_SERVER,
  SELECTED_DOMAIN,
  ACCOUNT_SETTINGS,
  INFRASTRUCTURE_CONSOLE,
  MAIN_ACCOUNTING_SERVER,
  INVOICING_SERVER,
  SELECTED_INVOICE_SERVER,
} from '../../../constants/storage';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  authorizationHeader: HttpHeaders;
  constructor(
    private readonly http: HttpClient,
    private readonly oauthService: OAuthService,
  ) {
    this.authorizationHeader = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }
  clearInfoLocalStorage() {
    localStorage.removeItem(CLIENT_ID);
    localStorage.removeItem(REDIRECT_URI);
    localStorage.removeItem(SILENT_REFRESH_REDIRECT_URI);
    localStorage.removeItem(LOGIN_URL);
    localStorage.removeItem(ISSUER_URL);
    localStorage.removeItem(USER_UUID);
  }

  setInfoLocalStorage(response) {
    localStorage.setItem(CLIENT_ID, response.clientId);
    localStorage.setItem(REDIRECT_URI, response.callbackURLs[0]);
    localStorage.setItem(SILENT_REFRESH_REDIRECT_URI, response.callbackURLs[1]);
    localStorage.setItem(LOGIN_URL, response.authorizationURL);
    localStorage.setItem(ISSUER_URL, response.authServerURL);
    localStorage.setItem(APP_URL, response.appURL);
    localStorage.setItem(SERVICES, JSON.stringify(response.services));
    this.gerServiceUrl().subscribe({
      next: data => {
        if (data.length > 0) {
          data.forEach(async element => {
            await response.services.push({
              type: element.type,
              url: element.serviceURL,
              name: element.name,
            });
          });
        }
        localStorage.setItem(SERVICES, JSON.stringify(response.services));
        localStorage.setItem(
          SELECTED_DOMAIN,
          this.getServiceURL(MAIN_ACCOUNTING_SERVER) ||
            this.getServiceURL(ACCOUNTING_SERVER),
        );
        const server = this.getServiceURL(INVOICING_SERVER);
        localStorage.setItem(SELECTED_INVOICE_SERVER, server);
      },
      error: err => {},
    });
  }

  getInfo(key) {
    return localStorage.getItem(key);
  }

  getSelectedDomainSettings() {
    const domain = this.getInfo(SELECTED_DOMAIN);
    return this.http.get(domain + '/info');
  }

  getServiceURL(serviceName) {
    const services = JSON.parse(localStorage.getItem(SERVICES) || '[]');
    for (const service of services) {
      if (service.type === serviceName) {
        return service.url;
      }
    }
  }

  getSelectedDomain(server?) {
    let serviceURL = localStorage.getItem(SELECTED_DOMAIN);
    if (server) {
      serviceURL = localStorage.getItem(SELECTED_INVOICE_SERVER);
    }
    const services = JSON.parse(localStorage.getItem(SERVICES) || '[]');
    for (const service of services) {
      if (service.url === serviceURL) {
        return service;
      }
    }
  }

  gerServiceUrl(): Observable<any> {
    const infrastructureUrl =
      this.getServiceURL(INFRASTRUCTURE_CONSOLE) + '/service/v1/list?type=';
    const accountingRequestUrl = infrastructureUrl + ACCOUNTING_SERVER;
    const invoicingRequestUrl = infrastructureUrl + INVOICING_SERVER;
    return this.http.get(accountingRequestUrl).pipe(
      mergeMap((accountingServices: any) => {
        return this.http.get(invoicingRequestUrl).pipe(
          map((invoicingServices: any) => {
            const services = invoicingServices.docs.concat(
              accountingServices.docs,
            );
            return services;
          }),
        );
      }),
    );
  }

  setSelectedDomain(url, server?) {
    if (server) {
      localStorage.setItem(server, url.url);
    } else {
      localStorage.setItem(SELECTED_DOMAIN, url.url);
    }
  }

  getDomainSettings(url?) {
    const server = url || this.getSelectedDomain();
    const services = this.getDomainList(INVOICING_SERVER);
    for (const service of services) {
      this.http
        .get(service.url + '/api/settings/v1/getSettings', {
          headers: this.authorizationHeader,
        })
        .subscribe({
          next: (response: any) => {
            if (response.accountingServer === server.url) {
              return response.accountingServer;
            }
          },
          error: err => {
            return { message: 'Error in fetching invoice settings' };
          },
        });
    }
    return { message: 'Invoice Server Setup Incomplete On Selected Domain' };
  }

  getDomainList(serviceName) {
    const servicesList = [];
    const services = JSON.parse(localStorage.getItem(SERVICES) || '[]');
    for (const service of services) {
      if (service.type.endsWith(serviceName)) {
        servicesList.push(service);
      }
    }
    return servicesList;
  }
  domainJson(param) {
    const servicesList = [];
    const services = JSON.parse(localStorage.getItem(SERVICES) || '[]');
    for (const service of services) {
      if (
        service.type === param ||
        service.url === param ||
        service.name === param
      ) {
        servicesList.push(service);
      }
    }
    return servicesList;
  }

  getAccountingInfo() {
    const service = this.getDomainList(ACCOUNTING_SERVER);
    const accountSettings = [];
    return from(service).pipe(
      mergeMap(data => {
        return this.http
          .get<any>(data.url + '/info')
          .pipe(
            map(account => {
              accountSettings.push({
                type: data.type,
                accountSetup: account.accountSetup,
                periodClosing: account.periodClosing,
              });
            }),
            catchError(error => {
              return error;
            }),
          )
          .pipe(
            map((serverSettings: any) => {
              localStorage.setItem(
                ACCOUNT_SETTINGS,
                JSON.stringify(accountSettings),
              );
            }),
          );
      }),
    );
  }
}
