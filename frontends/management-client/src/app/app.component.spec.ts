import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { oauthServiceStub } from './common/testing-helpers';
import { of } from 'rxjs';
import { Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared-imports/material/material.module';
import { StorageService } from './common/services/storage-service/storage.service';

@Component({ selector: 'app-navigation', template: '' })
class NavigationComponent {}

const appServiceStub: Partial<AppService> = {
  getMessage: () => {
    return of();
  },
};

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, MaterialModule, BrowserAnimationsModule],
      declarations: [AppComponent, NavigationComponent],
      providers: [
        {
          provide: AppService,
          useValue: appServiceStub,
        },
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: StorageService,
          useValue: {
            getInfo: (...args) => {},
            getItem: (...args) => {},
            setInfoLocalStorage: (...args) => {},
          },
        },
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
