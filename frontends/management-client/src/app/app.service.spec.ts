import { TestBed, getTestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { AppService } from './app.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { oauthServiceStub } from './common/testing-helpers';
import { MatSnackBar } from '@angular/material';

describe('AppService', () => {
  let injector: TestBed;
  let service: AppService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AppService,
        {
          provide: Storage,
          useValue: {},
        },
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: MatSnackBar,
          useValue: {},
        },
      ],
    });

    injector = getTestBed();
    service = injector.get(AppService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  afterEach(() => {
    httpMock.verify();
  });
});
