#!/bin/bash

cd /tmp

# Clone building blocks configuration
git clone https://github.com/castlecraft/helm-charts

# Check helm chart is installed or create
# reuse installed values and resets data
export CHECK_MAN=$(helm ls -q stg-man-cc-in --tiller-namespace staging)
if [ "$CHECK_MAN" = "stg-man-cc-in" ]
then
    echo "Updating existing stg-man-cc-in . . ."
    helm upgrade stg-man-cc-in \
        --tiller-namespace staging \
        --namespace staging \
        --reuse-values \
        helm-charts/management-tools/management-console
fi

export CHECK_AC=$(helm ls -q stg-acc-cc-in --tiller-namespace staging)
if [ "$CHECK_AC" = "stg-acc-cc-in" ]
then
    echo "Updating existing stg-acc-cc-in . . ."
    helm upgrade stg-acc-cc-in \
        --tiller-namespace staging \
        --namespace staging \
        --reuse-values \
        helm-charts/management-tools/accounting-server
fi

export CHECK_AC2=$(helm ls -q stg-acc2-cc-in --tiller-namespace staging)
if [ "$CHECK_AC2" = "stg-acc2-cc-in" ]
then
    echo "Updating existing stg-acc2-cc-in . . ."
    helm upgrade stg-acc2-cc-in \
        --tiller-namespace staging \
        --namespace staging \
        --reuse-values \
        helm-charts/management-tools/accounting-server
fi

export CHECK_INV=$(helm ls -q stg-inv-cc-in --tiller-namespace staging)
if [ "$CHECK_INV" = "stg-inv-cc-in" ]
then
    echo "Updating existing stg-inv-cc-in . . ."
    helm upgrade stg-inv-cc-in \
        --tiller-namespace staging \
        --namespace staging \
        --reuse-values \
        helm-charts/management-tools/invoice-server
fi

export CHECK_CST=$(helm ls -q stg-cst-cc-in --tiller-namespace staging)
if [ "$CHECK_CST" = "stg-cst-cc-in" ]
then
    echo "Updating existing stg-cst-cc-in . . ."
    helm upgrade stg-cst-cc-in \
        --tiller-namespace staging \
        --namespace staging \
        --reuse-values \
        helm-charts/management-tools/customer-service
fi
