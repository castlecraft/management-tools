#!/usr/bin/env bash

# Update packages

# Root Dependencies
./node_modules/.bin/npm-check --update

# Accounting Server
./node_modules/.bin/npm-check --update packages/accounting-server

# Management Console
./node_modules/.bin/npm-check --update packages/management-console

# Customer Service
./node_modules/.bin/npm-check --update packages/customer-service

# Invoice Server
./node_modules/.bin/npm-check --update packages/invoice-server

# Management Client
./node_modules/.bin/npm-check --update frontend/management-client
