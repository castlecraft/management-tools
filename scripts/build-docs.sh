#!/usr/bin/env bash
echo "Building Accounting Server Docs"
./node_modules/.bin/typedoc\
    --out public/api/accounting-server \
    packages/accounting-server/src \
    --name "Accounting Server"

echo "Building Management Console Docs"
./node_modules/.bin/typedoc \
    --out public/api/management-console-server \
    packages/management-console/server \
    --name "Management Console server"

echo "Building Invocie Server Docs"
./node_modules/.bin/typedoc\
    --out public/api/invoice-server \
    packages/invoice-server/src \
    --name "Invoice Server"

echo "Building Customer Service Docs"
./node_modules/.bin/typedoc\
    --out public/api/customer-service \
    packages/customer-service/src \
    --name "Customer Service"
